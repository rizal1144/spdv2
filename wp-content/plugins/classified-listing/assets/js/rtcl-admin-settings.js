/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 19);
/******/ })
/************************************************************************/
/******/ ({

/***/ 19:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(20);


/***/ }),

/***/ 20:
/***/ (function(module, exports) {

eval("+function ($) {\n    'use-strict';\n\n    $(function () {\n        if ($.fn.select2) {\n            $('.rtcl-select2').select2();\n        }\n        if ($.fn.wpColorPicker) {\n            $('.rtcl-color').wpColorPicker();\n        }\n\n        $('.rtcl-setting-image-wrap').on('click', '.rtcl-add-image', function (e) {\n\n            e.preventDefault();\n\n            var self = $(this),\n                target = self.parents('.rtcl-setting-image-wrap'),\n                file_frame,\n                image_data,\n                json;\n\n            // If an instance of file_frame already exists, then we can open it rather than creating a new instance\n            if (undefined !== file_frame) {\n                file_frame.open();\n                return;\n            }\n\n            // Here, use the wp.media library to define the settings of the media uploader\n            file_frame = wp.media.frames.file_frame = wp.media({\n                frame: 'post',\n                state: 'insert',\n                multiple: false\n            });\n\n            // Setup an event handler for what to do when an image has been selected\n            file_frame.on('insert', function () {\n\n                // Read the JSON data returned from the media uploader\n                json = file_frame.state().get('selection').first().toJSON();\n                // First, make sure that we have the URL of an image to display\n                if (0 > $.trim(json.url.length)) {\n                    return;\n                }\n                var imgUrl = typeof json.sizes.medium === \"undefined\" ? json.url : json.sizes.medium.url;\n                target.find('.rtcl-setting-image-id').val(json.id);\n                target.find('.image-preview-wrapper').html('<img src=\"' + imgUrl + '\" alt=\"' + json.title + '\" />');\n            });\n\n            // Now display the actual file_frame\n            file_frame.open();\n        });\n\n        // Delete the image when \"Remove Image\" button clicked\n        $('.rtcl-setting-image-wrap').on('click', '.rtcl-remove-image', function (e) {\n            e.preventDefault();\n            var self = $(this),\n                target = self.parents('.rtcl-setting-image-wrap');\n            if (confirm('Are you sure to delete?')) {\n                target.find('.rtcl-setting-image-id').val('');\n                target.find('.image-preview-wrapper img').attr('src', target.find('.image-preview-wrapper').data('placeholder'));\n            }\n        });\n\n        var enable_terms_conditions = $(\"#rtcl_account_settings-enable_terms_conditions\");\n        if (enable_terms_conditions.is(\":checked\")) {\n            $('.rtcl_account_settings-terms_conditions').show('slow');\n        }\n        enable_terms_conditions.on('change', function () {\n            if ($(this).is(\":checked\")) {\n                $('.rtcl_account_settings-terms_conditions').show('slow');\n            } else {\n                $('.rtcl_account_settings-terms_conditions').hide('slow');\n            }\n        });\n    });\n}(jQuery);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvanMvYWRtaW4tc2V0dGluZ3MuanM/YzY5MSJdLCJuYW1lcyI6WyIkIiwiZm4iLCJzZWxlY3QyIiwid3BDb2xvclBpY2tlciIsIm9uIiwiZSIsInByZXZlbnREZWZhdWx0Iiwic2VsZiIsInRhcmdldCIsInBhcmVudHMiLCJmaWxlX2ZyYW1lIiwiaW1hZ2VfZGF0YSIsImpzb24iLCJ1bmRlZmluZWQiLCJvcGVuIiwid3AiLCJtZWRpYSIsImZyYW1lcyIsImZyYW1lIiwic3RhdGUiLCJtdWx0aXBsZSIsImdldCIsImZpcnN0IiwidG9KU09OIiwidHJpbSIsInVybCIsImxlbmd0aCIsImltZ1VybCIsInNpemVzIiwibWVkaXVtIiwiZmluZCIsInZhbCIsImlkIiwiaHRtbCIsInRpdGxlIiwiY29uZmlybSIsImF0dHIiLCJkYXRhIiwiZW5hYmxlX3Rlcm1zX2NvbmRpdGlvbnMiLCJpcyIsInNob3ciLCJoaWRlIiwialF1ZXJ5Il0sIm1hcHBpbmdzIjoiQUFBQSxDQUFFLFVBQVVBLENBQVYsRUFBYTtBQUNYOztBQUVBQSxNQUFFLFlBQVk7QUFDVixZQUFJQSxFQUFFQyxFQUFGLENBQUtDLE9BQVQsRUFBa0I7QUFDZEYsY0FBRSxlQUFGLEVBQW1CRSxPQUFuQjtBQUNIO0FBQ0QsWUFBSUYsRUFBRUMsRUFBRixDQUFLRSxhQUFULEVBQXdCO0FBQ3BCSCxjQUFFLGFBQUYsRUFBaUJHLGFBQWpCO0FBQ0g7O0FBRURILFVBQUUsMEJBQUYsRUFBOEJJLEVBQTlCLENBQWlDLE9BQWpDLEVBQTBDLGlCQUExQyxFQUE2RCxVQUFVQyxDQUFWLEVBQWE7O0FBRXRFQSxjQUFFQyxjQUFGOztBQUVBLGdCQUFJQyxPQUFPUCxFQUFFLElBQUYsQ0FBWDtBQUFBLGdCQUNJUSxTQUFTRCxLQUFLRSxPQUFMLENBQWEsMEJBQWIsQ0FEYjtBQUFBLGdCQUVJQyxVQUZKO0FBQUEsZ0JBRWdCQyxVQUZoQjtBQUFBLGdCQUU0QkMsSUFGNUI7O0FBSUE7QUFDQSxnQkFBSUMsY0FBY0gsVUFBbEIsRUFBOEI7QUFDMUJBLDJCQUFXSSxJQUFYO0FBQ0E7QUFDSDs7QUFFRDtBQUNBSix5QkFBYUssR0FBR0MsS0FBSCxDQUFTQyxNQUFULENBQWdCUCxVQUFoQixHQUE2QkssR0FBR0MsS0FBSCxDQUFTO0FBQy9DRSx1QkFBTyxNQUR3QztBQUUvQ0MsdUJBQU8sUUFGd0M7QUFHL0NDLDBCQUFVO0FBSHFDLGFBQVQsQ0FBMUM7O0FBTUE7QUFDQVYsdUJBQVdOLEVBQVgsQ0FBYyxRQUFkLEVBQXdCLFlBQVk7O0FBRWhDO0FBQ0FRLHVCQUFPRixXQUFXUyxLQUFYLEdBQW1CRSxHQUFuQixDQUF1QixXQUF2QixFQUFvQ0MsS0FBcEMsR0FBNENDLE1BQTVDLEVBQVA7QUFDQTtBQUNBLG9CQUFJLElBQUl2QixFQUFFd0IsSUFBRixDQUFPWixLQUFLYSxHQUFMLENBQVNDLE1BQWhCLENBQVIsRUFBaUM7QUFDN0I7QUFDSDtBQUNELG9CQUFJQyxTQUFVLE9BQU9mLEtBQUtnQixLQUFMLENBQVdDLE1BQWxCLEtBQTZCLFdBQTlCLEdBQTZDakIsS0FBS2EsR0FBbEQsR0FBd0RiLEtBQUtnQixLQUFMLENBQVdDLE1BQVgsQ0FBa0JKLEdBQXZGO0FBQ0FqQix1QkFBT3NCLElBQVAsQ0FBWSx3QkFBWixFQUFzQ0MsR0FBdEMsQ0FBMENuQixLQUFLb0IsRUFBL0M7QUFDQXhCLHVCQUFPc0IsSUFBUCxDQUFZLHdCQUFaLEVBQXNDRyxJQUF0QyxDQUEyQyxlQUFlTixNQUFmLEdBQXdCLFNBQXhCLEdBQW9DZixLQUFLc0IsS0FBekMsR0FBaUQsTUFBNUY7QUFFSCxhQVpEOztBQWNBO0FBQ0F4Qix1QkFBV0ksSUFBWDtBQUVILFNBdkNEOztBQXlDQTtBQUNBZCxVQUFFLDBCQUFGLEVBQThCSSxFQUE5QixDQUFpQyxPQUFqQyxFQUEwQyxvQkFBMUMsRUFBZ0UsVUFBVUMsQ0FBVixFQUFhO0FBQ3pFQSxjQUFFQyxjQUFGO0FBQ0EsZ0JBQUlDLE9BQU9QLEVBQUUsSUFBRixDQUFYO0FBQUEsZ0JBQ0lRLFNBQVNELEtBQUtFLE9BQUwsQ0FBYSwwQkFBYixDQURiO0FBRUEsZ0JBQUkwQixRQUFRLHlCQUFSLENBQUosRUFBd0M7QUFDcEMzQix1QkFBT3NCLElBQVAsQ0FBWSx3QkFBWixFQUFzQ0MsR0FBdEMsQ0FBMEMsRUFBMUM7QUFDQXZCLHVCQUFPc0IsSUFBUCxDQUFZLDRCQUFaLEVBQTBDTSxJQUExQyxDQUErQyxLQUEvQyxFQUFzRDVCLE9BQU9zQixJQUFQLENBQVksd0JBQVosRUFBc0NPLElBQXRDLENBQTJDLGFBQTNDLENBQXREO0FBQ0g7QUFDSixTQVJEOztBQVVBLFlBQUlDLDBCQUEwQnRDLEVBQUUsZ0RBQUYsQ0FBOUI7QUFDQSxZQUFJc0Msd0JBQXdCQyxFQUF4QixDQUEyQixVQUEzQixDQUFKLEVBQTRDO0FBQ3hDdkMsY0FBRSx5Q0FBRixFQUE2Q3dDLElBQTdDLENBQWtELE1BQWxEO0FBQ0g7QUFDREYsZ0NBQXdCbEMsRUFBeEIsQ0FBMkIsUUFBM0IsRUFBcUMsWUFBWTtBQUM3QyxnQkFBSUosRUFBRSxJQUFGLEVBQVF1QyxFQUFSLENBQVcsVUFBWCxDQUFKLEVBQTRCO0FBQ3hCdkMsa0JBQUUseUNBQUYsRUFBNkN3QyxJQUE3QyxDQUFrRCxNQUFsRDtBQUNILGFBRkQsTUFFTztBQUNIeEMsa0JBQUUseUNBQUYsRUFBNkN5QyxJQUE3QyxDQUFrRCxNQUFsRDtBQUNIO0FBQ0osU0FORDtBQVFILEtBeEVEO0FBMEVILENBN0VBLENBNkVFQyxNQTdFRixDQUFEIiwiZmlsZSI6IjIwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiKyhmdW5jdGlvbiAoJCkge1xuICAgICd1c2Utc3RyaWN0JztcblxuICAgICQoZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoJC5mbi5zZWxlY3QyKSB7XG4gICAgICAgICAgICAkKCcucnRjbC1zZWxlY3QyJykuc2VsZWN0MigpO1xuICAgICAgICB9XG4gICAgICAgIGlmICgkLmZuLndwQ29sb3JQaWNrZXIpIHtcbiAgICAgICAgICAgICQoJy5ydGNsLWNvbG9yJykud3BDb2xvclBpY2tlcigpO1xuICAgICAgICB9XG5cbiAgICAgICAgJCgnLnJ0Y2wtc2V0dGluZy1pbWFnZS13cmFwJykub24oJ2NsaWNrJywgJy5ydGNsLWFkZC1pbWFnZScsIGZ1bmN0aW9uIChlKSB7XG5cbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgICAgdmFyIHNlbGYgPSAkKHRoaXMpLFxuICAgICAgICAgICAgICAgIHRhcmdldCA9IHNlbGYucGFyZW50cygnLnJ0Y2wtc2V0dGluZy1pbWFnZS13cmFwJyksXG4gICAgICAgICAgICAgICAgZmlsZV9mcmFtZSwgaW1hZ2VfZGF0YSwganNvbjtcblxuICAgICAgICAgICAgLy8gSWYgYW4gaW5zdGFuY2Ugb2YgZmlsZV9mcmFtZSBhbHJlYWR5IGV4aXN0cywgdGhlbiB3ZSBjYW4gb3BlbiBpdCByYXRoZXIgdGhhbiBjcmVhdGluZyBhIG5ldyBpbnN0YW5jZVxuICAgICAgICAgICAgaWYgKHVuZGVmaW5lZCAhPT0gZmlsZV9mcmFtZSkge1xuICAgICAgICAgICAgICAgIGZpbGVfZnJhbWUub3BlbigpO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8gSGVyZSwgdXNlIHRoZSB3cC5tZWRpYSBsaWJyYXJ5IHRvIGRlZmluZSB0aGUgc2V0dGluZ3Mgb2YgdGhlIG1lZGlhIHVwbG9hZGVyXG4gICAgICAgICAgICBmaWxlX2ZyYW1lID0gd3AubWVkaWEuZnJhbWVzLmZpbGVfZnJhbWUgPSB3cC5tZWRpYSh7XG4gICAgICAgICAgICAgICAgZnJhbWU6ICdwb3N0JyxcbiAgICAgICAgICAgICAgICBzdGF0ZTogJ2luc2VydCcsXG4gICAgICAgICAgICAgICAgbXVsdGlwbGU6IGZhbHNlXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgLy8gU2V0dXAgYW4gZXZlbnQgaGFuZGxlciBmb3Igd2hhdCB0byBkbyB3aGVuIGFuIGltYWdlIGhhcyBiZWVuIHNlbGVjdGVkXG4gICAgICAgICAgICBmaWxlX2ZyYW1lLm9uKCdpbnNlcnQnLCBmdW5jdGlvbiAoKSB7XG5cbiAgICAgICAgICAgICAgICAvLyBSZWFkIHRoZSBKU09OIGRhdGEgcmV0dXJuZWQgZnJvbSB0aGUgbWVkaWEgdXBsb2FkZXJcbiAgICAgICAgICAgICAgICBqc29uID0gZmlsZV9mcmFtZS5zdGF0ZSgpLmdldCgnc2VsZWN0aW9uJykuZmlyc3QoKS50b0pTT04oKTtcbiAgICAgICAgICAgICAgICAvLyBGaXJzdCwgbWFrZSBzdXJlIHRoYXQgd2UgaGF2ZSB0aGUgVVJMIG9mIGFuIGltYWdlIHRvIGRpc3BsYXlcbiAgICAgICAgICAgICAgICBpZiAoMCA+ICQudHJpbShqc29uLnVybC5sZW5ndGgpKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdmFyIGltZ1VybCA9ICh0eXBlb2YganNvbi5zaXplcy5tZWRpdW0gPT09IFwidW5kZWZpbmVkXCIpID8ganNvbi51cmwgOiBqc29uLnNpemVzLm1lZGl1bS51cmw7XG4gICAgICAgICAgICAgICAgdGFyZ2V0LmZpbmQoJy5ydGNsLXNldHRpbmctaW1hZ2UtaWQnKS52YWwoanNvbi5pZCk7XG4gICAgICAgICAgICAgICAgdGFyZ2V0LmZpbmQoJy5pbWFnZS1wcmV2aWV3LXdyYXBwZXInKS5odG1sKCc8aW1nIHNyYz1cIicgKyBpbWdVcmwgKyAnXCIgYWx0PVwiJyArIGpzb24udGl0bGUgKyAnXCIgLz4nKTtcblxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIC8vIE5vdyBkaXNwbGF5IHRoZSBhY3R1YWwgZmlsZV9mcmFtZVxuICAgICAgICAgICAgZmlsZV9mcmFtZS5vcGVuKCk7XG5cbiAgICAgICAgfSk7XG5cbiAgICAgICAgLy8gRGVsZXRlIHRoZSBpbWFnZSB3aGVuIFwiUmVtb3ZlIEltYWdlXCIgYnV0dG9uIGNsaWNrZWRcbiAgICAgICAgJCgnLnJ0Y2wtc2V0dGluZy1pbWFnZS13cmFwJykub24oJ2NsaWNrJywgJy5ydGNsLXJlbW92ZS1pbWFnZScsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICB2YXIgc2VsZiA9ICQodGhpcyksXG4gICAgICAgICAgICAgICAgdGFyZ2V0ID0gc2VsZi5wYXJlbnRzKCcucnRjbC1zZXR0aW5nLWltYWdlLXdyYXAnKTtcbiAgICAgICAgICAgIGlmIChjb25maXJtKCdBcmUgeW91IHN1cmUgdG8gZGVsZXRlPycpKSB7XG4gICAgICAgICAgICAgICAgdGFyZ2V0LmZpbmQoJy5ydGNsLXNldHRpbmctaW1hZ2UtaWQnKS52YWwoJycpO1xuICAgICAgICAgICAgICAgIHRhcmdldC5maW5kKCcuaW1hZ2UtcHJldmlldy13cmFwcGVyIGltZycpLmF0dHIoJ3NyYycsIHRhcmdldC5maW5kKCcuaW1hZ2UtcHJldmlldy13cmFwcGVyJykuZGF0YSgncGxhY2Vob2xkZXInKSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGxldCBlbmFibGVfdGVybXNfY29uZGl0aW9ucyA9ICQoXCIjcnRjbF9hY2NvdW50X3NldHRpbmdzLWVuYWJsZV90ZXJtc19jb25kaXRpb25zXCIpO1xuICAgICAgICBpZiAoZW5hYmxlX3Rlcm1zX2NvbmRpdGlvbnMuaXMoXCI6Y2hlY2tlZFwiKSkge1xuICAgICAgICAgICAgJCgnLnJ0Y2xfYWNjb3VudF9zZXR0aW5ncy10ZXJtc19jb25kaXRpb25zJykuc2hvdygnc2xvdycpO1xuICAgICAgICB9XG4gICAgICAgIGVuYWJsZV90ZXJtc19jb25kaXRpb25zLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBpZiAoJCh0aGlzKS5pcyhcIjpjaGVja2VkXCIpKSB7XG4gICAgICAgICAgICAgICAgJCgnLnJ0Y2xfYWNjb3VudF9zZXR0aW5ncy10ZXJtc19jb25kaXRpb25zJykuc2hvdygnc2xvdycpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAkKCcucnRjbF9hY2NvdW50X3NldHRpbmdzLXRlcm1zX2NvbmRpdGlvbnMnKS5oaWRlKCdzbG93Jyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgfSk7XG5cbn0pKGpRdWVyeSk7XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2pzL2FkbWluLXNldHRpbmdzLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///20\n");

/***/ })

/******/ });