<?php

use Rtcl\Helpers\Functions;
use Rtcl\Resources\Options;

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Settings for Payment
 */
$options = array(
    'enable_myaccount_registration' => array(
        'title'       => __('Account creation', 'classified-listing'),
        'type'        => 'checkbox',
        'default'     => 'yes',
        'description' => __('Allow customers to create an account on the "My account" page', 'classified-listing'),
    ),
    'user_role'                     => array(
        'title'      => __('New User Default Role', 'classified-listing'),
        'type'       => 'select',
        'class'      => 'rtcl-select2',
        'blank_text' => __("Default Role as wordpress", 'classified-listing'),
        'options'    => Functions::get_user_roles(),
        'css'        => 'min-width:300px;'
    ),
    'terms_conditions_section'      => array(
        'title'       => __('Terms and conditions', 'classified-listing'),
        'type'        => 'title',
        'description' => '',
    ),
    'enable_terms_conditions'       => array(
        'title'       => __('Enable Terms and conditions', 'classified-listing'),
        'type'        => 'checkbox',
        'description' => __("Display and require user agreement to Terms and Conditions", 'classified-listing')
    ),
    'terms_conditions'              => array(
        'title'         => __('Terms and conditions', 'classified-listing'),
        'type'          => 'textarea',
        'wrapper_class' => Functions::get_option_item('rtcl_account_settings', 'enable_terms_conditions', null, 'checkbox') ? '' : 'hidden',
        'default'       => __('I have read and agree to the <a href="#">Terms and Conditions</a> and <a href="#">Privacy Policy</a>', 'classified-listing'),
        'description'   => __('HTML hints: only anchor(a) tag is allowed', 'classified-listing')
    )
);

return apply_filters('rtcl_account_settings_options', $options);