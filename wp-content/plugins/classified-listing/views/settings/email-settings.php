<?php

use Rtcl\Helpers\Functions;
use Rtcl\Resources\Options;

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Settings for Payment
 */
$options = array(
    'gs_section'                 => array(
        'title'       => __('General settings', 'classified-listing'),
        'type'        => 'title',
        'description' => '',
    ),
    'from_name'                  => array(
        'title'       => __('From name', 'classified-listing'),
        'type'        => 'text',
        'default'     => get_option('blogname'),
        'description' => __('The name system generated emails are sent from. This should probably be your site or directory name.',
            'classified-listing')
    ),
    'from_email'                 => array(
        'title'   => __('From email', 'classified-listing'),
        'type'    => 'text',
        'default' => get_option('admin_email')
    ),
    'admin_notice_emails'        => array(
        'title'       => __('Admin notification emails', 'classified-listing'),
        'type'        => 'textarea',
        'default'     => get_option('admin_email'),
        'description' => __('Enter the email address(es) that should receive admin notification emails, one per line.',
            'classified-listing')
    ),
    'notify_admin'               => array(
        'title'   => __('Notify admin via email when', 'classified-listing'),
        'type'    => 'multi_checkbox',
        'default' => array('register_new_user', 'listing_submitted', 'order_created', 'payment_received'),
        'options' => array(
            'register_new_user' => __('A new user is registered (Only work when user registered using Classified listing plugin registration form)', 'classified-listing'),
            'listing_submitted' => __('A new listing is submitted', 'classified-listing'),
            'listing_edited'    => __('A listing is edited', 'classified-listing'),
            'listing_expired'   => __('A listing expired', 'classified-listing'),
            'order_created'     => __('Order created', 'classified-listing'),
            'order_completed'   => __('Payment received / Order Completed', 'classified-listing'),
            'listing_contact'   => __('A contact message is sent to a listing owner', 'classified-listing')
        )
    ),
    'notify_users'               => array(
        'title'   => __('Notify users via email when their', 'classified-listing'),
        'type'    => 'multi_checkbox',
        'default' => array(
            'listing_submitted',
            'listing_published',
            'listing_renewal',
            'listing_expired',
            'remind_renewal',
            'order_created',
            'order_completed'
        ),
        'options' => array(
            'listing_submitted' => __('Listing is submitted', 'classified-listing'),
            'listing_published' => __('Listing is approved/published', 'classified-listing'),
            'listing_renewal'   => __('Listing is about to expire (reached renewal email threshold)', 'classified-listing'),
            'listing_expired'   => __('Listing expired', 'classified-listing'),
            'remind_renewal'    => __('Listing expired and reached renewal reminder email threshold', 'classified-listing'),
            'order_created'     => __('Order created', 'classified-listing'),
            'order_completed'   => __('Order completed', 'classified-listing')
        )
    ),
    'email_info_section'         => array(
        'title'       => __('HTML is accepted. You can use the following placeholders:',
            'classified-listing'),
        'type'        => 'title',
        'description' => '{name} - ' . __('The listing owner\'s display name on the site',
                'classified-listing') . '<br>' .
            '{username} - ' . __('The listing owner\'s user name on the site',
                'classified-listing') . '<br>' .
            '{site_name} - ' . __('Your site name', 'classified-listing') . '<br>' .
            '{site_link} - ' . __('Your site name with link', 'classified-listing') . '<br>' .
            '{site_url} - ' . __('Your site url with link', 'classified-listing') . '<br>' .
            '{listing_title} - ' . __('Listing\'s title', 'classified-listing') . '<br>' .
            '{listing_link} - ' . __('Listing\'s title with link', 'classified-listing') . '<br>' .
            '{listing_url} - ' . __('Listing\'s url with link', 'classified-listing') . '<br>' .
            '{sender_name} - ' . __('Sender\'s name', 'classified-listing') . '<br>' .
            '{sender_email} - ' . __('Sender\'s email address', 'classified-listing') . '<br>' .
            '{message} - ' . __('Contact message', 'classified-listing') . '<br>' .
            '{expiration_date} - ' . __('Expiration date', 'classified-listing') . '<br>' .
            '{category_name} - ' . __('Category name that is going to expire', 'classified-listing') . '<br>' .
            '{renewal_link} - ' . __('Link to renewal page', 'classified-listing') . '<br>' .
            '{today} - ' . __('Current date', 'classified-listing') . '<br>' .
            '{now} - ' . __('Current time', 'classified-listing')
    ),
    'lscs_section'               => array(
        'title' => __('Listing submitted email ( confirmation )', 'classified-listing'),
        'type'  => 'title'
    ),
    'listing_submitted_subject'  => array(
        'title'   => __('Subject', 'classified-listing'),
        'type'    => 'text',
        'default' => __('[{site_name}] Listing "{listing_title}" received', 'classified-listing'),
    ),
    'listing_submitted_body'     => array(
        'title'   => __('Body', 'classified-listing'),
        'type'    => 'wysiwyg',
        'class'   => 'rtcl-wysiwyg',
        'default' => __("Dear {name},\n\nYour submission \"{listing_title}\" has been received and it's pending review. This review process could take up to 48 hours.\n\nThanks,\nThe Administrator of {site_name}",
            'classified-listing'),
    ),
    'lpas_section'               => array(
        'title' => __('Listing published/approved email', 'classified-listing'),
        'type'  => 'title',
    ),
    'listing_published_subject'  => array(
        'title'   => __('Subject', 'classified-listing'),
        'type'    => 'text',
        'default' => __('[{site_name}] Listing "{listing_title}" published', 'classified-listing'),
    ),
    'listing_published_body'     => array(
        'title'   => __('Body', 'classified-listing'),
        'type'    => 'wysiwyg',
        'class'   => 'rtcl-wysiwyg',
        'default' => __("Dear {name},\n\nYour listing \"{listing_title}\" is now available at {listing_url} and can be viewed by the public.\n\nThanks,\nThe Administrator of {site_name}",
            'classified-listing'),
    ),
    'renewal_section'            => array(
        'title' => __('Listing renewal email', 'classified-listing'),
        'type'  => 'title',
    ),
    'renewal_email_threshold'    => array(
        'title'       => __('Listing renewal email threshold (in days)', 'classified-listing'),
        'type'        => 'number',
        'default'     => 3,
        'description' => __('Configure how many days before listing expiration is the renewal email sent.', 'classified-listing')
    ),
    'renewal_subject'            => array(
        'title'   => __('Subject', 'classified-listing'),
        'type'    => 'text',
        'default' => __('[{site_name}] {listing_title} - Expiration notice', 'classified-listing'),
    ),
    'renewal_body'               => array(
        'title'   => __('Body', 'classified-listing'),
        'type'    => 'wysiwyg',
        'class'   => 'rtcl-wysiwyg',
        'default' => __("Dear {name},\n\nYour listing \"{listing_title}\" is about to expire at {site_link}. You can renew it here: {renewal_link}.\n\nThanks,\nThe Administrator of {site_name}", 'classified-listing')
    ),
    'expired_section'            => array(
        'title' => __('Listing expired email', 'classified-listing'),
        'type'  => 'title',
    ),
    'expired_subject'            => array(
        'title'   => __('Subject', 'classified-listing'),
        'type'    => 'text',
        'default' => __('[{site_name}] {listing_title} - Expiration notice', 'classified-listing'),
    ),
    'expired_body'               => array(
        'title'   => __('Body', 'classified-listing'),
        'type'    => 'wysiwyg',
        'class'   => 'rtcl-wysiwyg',
        'default' => __("Dear {name},\n\nYour listing \"{listing_title}\" in category \"{category_name}\" expired on {expiration_date}. To renew your listing click the link below.\n{renewal_link}\n\nThanks,\nThe Administrator of {site_name}", 'classified-listing')
    ),
    'renewal_reminder_section'   => array(
        'title' => __('Renewal reminder email', 'classified-listing'),
        'type'  => 'title',
    ),
    'renewal_reminder_threshold' => array(
        'title'       => __('Listing renewal reminder email threshold (in days)', 'classified-listing'),
        'type'        => 'number',
        'default'     => 3,
        'description' => __('Configure how many days after the expiration of a listing an email reminder should be sent to the owner.', 'classified-listing')
    ),
    'renewal_reminder_subject'   => array(
        'title'   => __('Subject', 'classified-listing'),
        'type'    => 'text',
        'default' => __('[{site_name}] {listing_title} - Expiration reminder', 'classified-listing'),
    ),
    'renewal_reminder_body'      => array(
        'title'   => __('Body', 'classified-listing'),
        'type'    => 'wysiwyg',
        'class'   => 'rtcl-wysiwyg',
        'default' => __("Dear {name},\n\nWe've noticed that you haven't renewed your listing \"{listing_title}\" for category \"{category_name}\" at {site_link} and just wanted to remind you that it expired on {expiration_date}. Please remember you can still renew it here: {renewal_link}.\n\nThanks,\nThe Administrator of {site_name}", 'classified-listing'),
    ),
    'o_created_s_section'        => array(
        'title' => __('Order created email', 'classified-listing'),
        'type'  => 'title',
    ),
    'order_created_subject'      => array(
        'title'   => __('Subject', 'classified-listing'),
        'type'    => 'text',
        'default' => __('[{site_name}] Thank you for your order', 'classified-listing')
    ),
    'order_created_body'         => array(
        'title'   => __('Body', 'classified-listing'),
        'type'    => 'wysiwyg',
        'class'   => 'rtcl-wysiwyg',
        'default' => __("Dear {name},\n\nThe order is now created.\n\nThis notification was for the order #{order_id} on the website {site_name}.\nYou can access the order details directly by clicking on the link below after logging in your account:\n{order_page}\n\n{order_details}\n\nThanks,\nThe Administrator of {site_name}",
            'classified-listing'),
    ),
    'ocs_section'                => array(
        'title' => __('Order completed email', 'classified-listing'),
        'type'  => 'title',
    ),
    'order_completed_subject'    => array(
        'title'   => __('Subject', 'classified-listing'),
        'type'    => 'text',
        'default' => __('[{site_name}] Order completed', 'classified-listing')
    ),
    'order_completed_body'       => array(
        'title'   => __('Body', 'classified-listing'),
        'type'    => 'wysiwyg',
        'class'   => 'rtcl-wysiwyg',
        'default' => __("Dear {name},\n\nYour recent order #{order_id} on {site_name} has been completed.\n\nYou can access the order details directly by clicking on the link below after logging in your account:\n{order_page}\n\n{order_details}\n\nThanks,\nThe Administrator of {site_name}",
            'classified-listing'),
    ),
    'lcs_section'                => array(
        'title' => __('Listing contact email', 'classified-listing'),
        'type'  => 'title',
    ),
    'contact_subject'            => array(
        'title'   => __('Subject', 'classified-listing'),
        'type'    => 'text',
        'default' => get_option('admin_email')
    ),
    'contact_body'               => array(
        'title' => __('Body', 'classified-listing'),
        'type'  => 'wysiwyg',
        'class' => 'rtcl-wysiwyg',
    ),
);

return apply_filters('rtcl_email_settings_options', $options);