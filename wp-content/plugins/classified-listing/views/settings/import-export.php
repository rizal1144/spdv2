<div class="wrap rtcl-import-export rtcl">
    <h1><?php _e( "Import Location", 'classified-listing' ) ?></h1>
    <div class="rtcl-ie-wrap" id="rtcl-import-wrap">
        <form class="form" id="rtcl-import-form">
            <div class="form-group row">
                <label for="rtcl-import-file"
                       class="rtcl-label col-sm-2 col-form-label"><?php _e( "Select Import File", "classified-listing" ) ?></label>
                <div class="col-sm-10">
                    <div class="col-sm-10 custom-file" style="width: 250px;">
                        <input type="file" class="custom-file-input rtcl-import-file" name="import-file" id="rtcl-import-file" required>
                        <label class="custom-file-label" for="rtcl-import-file"><?php _e( "Choose file...", "classified-listing" ) ?></label>
                    </div>
                </div>
            </div>

            <button class="btn btn-primary" type="submit"
                    id="rtcl-import-btn"><?php _e( "Import", "classified-listing" ) ?></button>
        </form>
        <div id="import-response" class="rtcl-hide">
            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
            </div>
            <div class="response"></div>
        </div>
    </div>
</div>