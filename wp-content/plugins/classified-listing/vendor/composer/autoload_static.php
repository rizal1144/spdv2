<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite69e37bc7faa6512b5c83ed06d5f4b5c
{
    public static $prefixLengthsPsr4 = array (
        'R' => 
        array (
            'Rtcl\\' => 5,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Rtcl\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app',
        ),
    );

    public static $classMap = array (
        'Rtcl\\Abstracts\\Session' => __DIR__ . '/../..' . '/app/Abstracts/Session.php',
        'Rtcl\\Controllers\\Admin\\AddConfig' => __DIR__ . '/../..' . '/app/Controllers/Admin/AddConfig.php',
        'Rtcl\\Controllers\\Admin\\AdminController' => __DIR__ . '/../..' . '/app/Controllers/Admin/AdminController.php',
        'Rtcl\\Controllers\\Admin\\AdminSettings' => __DIR__ . '/../..' . '/app/Controllers/Admin/AdminSettings.php',
        'Rtcl\\Controllers\\Admin\\Cron' => __DIR__ . '/../..' . '/app/Controllers/Admin/Cron.php',
        'Rtcl\\Controllers\\Admin\\Deactivator' => __DIR__ . '/../..' . '/app/Controllers/Admin/Deactivator.php',
        'Rtcl\\Controllers\\Admin\\EmailSettings' => __DIR__ . '/../..' . '/app/Controllers/Admin/EmailSettings.php',
        'Rtcl\\Controllers\\Admin\\Meta\\AddMetaBox' => __DIR__ . '/../..' . '/app/Controllers/Admin/Meta/AddMetaBox.php',
        'Rtcl\\Controllers\\Admin\\Meta\\AddTermMetaField' => __DIR__ . '/../..' . '/app/Controllers/Admin/Meta/AddTermMetaField.php',
        'Rtcl\\Controllers\\Admin\\Meta\\ListingMetaColumn' => __DIR__ . '/../..' . '/app/Controllers/Admin/Meta/ListingMetaColumn.php',
        'Rtcl\\Controllers\\Admin\\Meta\\ListingSubmitBoxMice' => __DIR__ . '/../..' . '/app/Controllers/Admin/Meta/ListingSubmitBoxMice.php',
        'Rtcl\\Controllers\\Admin\\Meta\\MetaController' => __DIR__ . '/../..' . '/app/Controllers/Admin/Meta/MetaController.php',
        'Rtcl\\Controllers\\Admin\\Meta\\PaymentColumn' => __DIR__ . '/../..' . '/app/Controllers/Admin/Meta/PaymentColumn.php',
        'Rtcl\\Controllers\\Admin\\Meta\\PricingMetaColumn' => __DIR__ . '/../..' . '/app/Controllers/Admin/Meta/PricingMetaColumn.php',
        'Rtcl\\Controllers\\Admin\\Meta\\RemoveMetaBox' => __DIR__ . '/../..' . '/app/Controllers/Admin/Meta/RemoveMetaBox.php',
        'Rtcl\\Controllers\\Admin\\Meta\\SaveCFGData' => __DIR__ . '/../..' . '/app/Controllers/Admin/Meta/SaveCFGData.php',
        'Rtcl\\Controllers\\Admin\\Meta\\SaveListingMetaData' => __DIR__ . '/../..' . '/app/Controllers/Admin/Meta/SaveListingMetaData.php',
        'Rtcl\\Controllers\\Admin\\Meta\\SavePricingMetaData' => __DIR__ . '/../..' . '/app/Controllers/Admin/Meta/SavePricingMetaData.php',
        'Rtcl\\Controllers\\Admin\\PaymentStatus' => __DIR__ . '/../..' . '/app/Controllers/Admin/PaymentStatus.php',
        'Rtcl\\Controllers\\Admin\\RegisterPostType' => __DIR__ . '/../..' . '/app/Controllers/Admin/RegisterPostType.php',
        'Rtcl\\Controllers\\Admin\\RestApi' => __DIR__ . '/../..' . '/app/Controllers/Admin/RestApi.php',
        'Rtcl\\Controllers\\Admin\\Roles' => __DIR__ . '/../..' . '/app/Controllers/Admin/Roles.php',
        'Rtcl\\Controllers\\Admin\\ScriptLoader' => __DIR__ . '/../..' . '/app/Controllers/Admin/ScriptLoader.php',
        'Rtcl\\Controllers\\Admin\\TemplateLoader' => __DIR__ . '/../..' . '/app/Controllers/Admin/TemplateLoader.php',
        'Rtcl\\Controllers\\Ajax\\Ajax' => __DIR__ . '/../..' . '/app/Controllers/Ajax/Ajax.php',
        'Rtcl\\Controllers\\Ajax\\AjaxCFG' => __DIR__ . '/../..' . '/app/Controllers/Ajax/AjaxCFG.php',
        'Rtcl\\Controllers\\Ajax\\AjaxGallery' => __DIR__ . '/../..' . '/app/Controllers/Ajax/AjaxGallery.php',
        'Rtcl\\Controllers\\Ajax\\Import' => __DIR__ . '/../..' . '/app/Controllers/Ajax/Import.php',
        'Rtcl\\Controllers\\Ajax\\ListingAdminAjax' => __DIR__ . '/../..' . '/app/Controllers/Ajax/ListingAdminAjax.php',
        'Rtcl\\Controllers\\Ajax\\PublicUser' => __DIR__ . '/../..' . '/app/Controllers/Ajax/PublicUser.php',
        'Rtcl\\Controllers\\FormHandler' => __DIR__ . '/../..' . '/app/Controllers/FormHandler.php',
        'Rtcl\\Controllers\\Hooks\\AdminHooks' => __DIR__ . '/../..' . '/app/Controllers/Hooks/AdminHooks.php',
        'Rtcl\\Controllers\\Hooks\\FrontEndHooks' => __DIR__ . '/../..' . '/app/Controllers/Hooks/FrontEndHooks.php',
        'Rtcl\\Controllers\\Hooks\\TemplateHooks' => __DIR__ . '/../..' . '/app/Controllers/Hooks/TemplateHooks.php',
        'Rtcl\\Controllers\\Install' => __DIR__ . '/../..' . '/app/Controllers/Install.php',
        'Rtcl\\Controllers\\ListingHook' => __DIR__ . '/../..' . '/app/Controllers/ListingHook.php',
        'Rtcl\\Controllers\\PublicAction' => __DIR__ . '/../..' . '/app/Controllers/PublicAction.php',
        'Rtcl\\Controllers\\Query' => __DIR__ . '/../..' . '/app/Controllers/Query.php',
        'Rtcl\\Controllers\\RtclPublic' => __DIR__ . '/../..' . '/app/Controllers/RtclPublic.php',
        'Rtcl\\Controllers\\SessionHandler' => __DIR__ . '/../..' . '/app/Controllers/SessionHandler.php',
        'Rtcl\\Controllers\\Shortcodes' => __DIR__ . '/../..' . '/app/Controllers/Shortcodes.php',
        'Rtcl\\Gateways\\Offline\\GatewayOffline' => __DIR__ . '/../..' . '/app/Gateways/Offline/GatewayOffline.php',
        'Rtcl\\Gateways\\Paypal\\GatewayPaypal' => __DIR__ . '/../..' . '/app/Gateways/Paypal/GatewayPaypal.php',
        'Rtcl\\Gateways\\Paypal\\lib\\GatewayPaypalRequest' => __DIR__ . '/../..' . '/app/Gateways/Paypal/lib/GatewayPaypalRequest.php',
        'Rtcl\\Gateways\\Paypal\\lib\\PayPalApiHandler' => __DIR__ . '/../..' . '/app/Gateways/Paypal/lib/PayPalApiHandler.php',
        'Rtcl\\Gateways\\Paypal\\lib\\PaypalRefund' => __DIR__ . '/../..' . '/app/Gateways/Paypal/lib/PaypalRefund.php',
        'Rtcl\\Helpers\\CacheHelper' => __DIR__ . '/../..' . '/app/Helpers/CacheHelper.php',
        'Rtcl\\Helpers\\Functions' => __DIR__ . '/../..' . '/app/Helpers/Functions.php',
        'Rtcl\\Helpers\\Helper' => __DIR__ . '/../..' . '/app/Helpers/Helper.php',
        'Rtcl\\Helpers\\Link' => __DIR__ . '/../..' . '/app/Helpers/Link.php',
        'Rtcl\\Helpers\\Pagination' => __DIR__ . '/../..' . '/app/Helpers/Pagination.php',
        'Rtcl\\Helpers\\SortImages' => __DIR__ . '/../..' . '/app/Helpers/SortImages.php',
        'Rtcl\\Helpers\\UploadHelper' => __DIR__ . '/../..' . '/app/Helpers/UploadHelper.php',
        'Rtcl\\Interfaces\\LoggerInterface' => __DIR__ . '/../..' . '/app/Interfaces/LoggerInterface.php',
        'Rtcl\\Log\\AbstractLogger' => __DIR__ . '/../..' . '/app/Log/AbstractLogger.php',
        'Rtcl\\Log\\InvalidArgumentException' => __DIR__ . '/../..' . '/app/Log/InvalidArgumentException.php',
        'Rtcl\\Log\\LogLevel' => __DIR__ . '/../..' . '/app/Log/LogLevel.php',
        'Rtcl\\Log\\Logger' => __DIR__ . '/../..' . '/app/Log/Logger.php',
        'Rtcl\\Log\\LoggerAwareInterface' => __DIR__ . '/../..' . '/app/Log/LoggerAwareInterface.php',
        'Rtcl\\Log\\LoggerAwareTrait' => __DIR__ . '/../..' . '/app/Log/LoggerAwareTrait.php',
        'Rtcl\\Log\\LoggerTrait' => __DIR__ . '/../..' . '/app/Log/LoggerTrait.php',
        'Rtcl\\Log\\NullLogger' => __DIR__ . '/../..' . '/app/Log/NullLogger.php',
        'Rtcl\\Models\\Listing' => __DIR__ . '/../..' . '/app/Models/Listing.php',
        'Rtcl\\Models\\Payment' => __DIR__ . '/../..' . '/app/Models/Payment.php',
        'Rtcl\\Models\\PaymentGateway' => __DIR__ . '/../..' . '/app/Models/PaymentGateway.php',
        'Rtcl\\Models\\PaymentGateways' => __DIR__ . '/../..' . '/app/Models/PaymentGateways.php',
        'Rtcl\\Models\\PaymentOption' => __DIR__ . '/../..' . '/app/Models/PaymentOption.php',
        'Rtcl\\Models\\RtclCFGField' => __DIR__ . '/../..' . '/app/Models/RtclCFGField.php',
        'Rtcl\\Models\\RtclEmail' => __DIR__ . '/../..' . '/app/Models/RtclEmail.php',
        'Rtcl\\Models\\SettingsAPI' => __DIR__ . '/../..' . '/app/Models/SettingsAPI.php',
        'Rtcl\\Models\\TermWalkerDropDown' => __DIR__ . '/../..' . '/app/Models/TermWalkerDropDown.php',
        'Rtcl\\Resources\\FieldGroup' => __DIR__ . '/../..' . '/app/Resources/FieldGroup.php',
        'Rtcl\\Resources\\FontAwesome' => __DIR__ . '/../..' . '/app/Resources/FontAwesome.php',
        'Rtcl\\Resources\\Gallery' => __DIR__ . '/../..' . '/app/Resources/Gallery.php',
        'Rtcl\\Resources\\ListingDetails' => __DIR__ . '/../..' . '/app/Resources/ListingDetails.php',
        'Rtcl\\Resources\\Options' => __DIR__ . '/../..' . '/app/Resources/Options.php',
        'Rtcl\\Resources\\PaymentOptions' => __DIR__ . '/../..' . '/app/Resources/PaymentOptions.php',
        'Rtcl\\Resources\\PricingOptions' => __DIR__ . '/../..' . '/app/Resources/PricingOptions.php',
        'Rtcl\\Shortcodes\\Categories' => __DIR__ . '/../..' . '/app/Shortcodes/Categories.php',
        'Rtcl\\Shortcodes\\Checkout' => __DIR__ . '/../..' . '/app/Shortcodes/Checkout.php',
        'Rtcl\\Shortcodes\\FilterListings' => __DIR__ . '/../..' . '/app/Shortcodes/FilterListings.php',
        'Rtcl\\Shortcodes\\ListingForm' => __DIR__ . '/../..' . '/app/Shortcodes/ListingForm.php',
        'Rtcl\\Shortcodes\\Listings' => __DIR__ . '/../..' . '/app/Shortcodes/Listings.php',
        'Rtcl\\Shortcodes\\MyAccount' => __DIR__ . '/../..' . '/app/Shortcodes/MyAccount.php',
        'Rtcl\\Widgets\\Categories' => __DIR__ . '/../..' . '/app/Widgets/Categories.php',
        'Rtcl\\Widgets\\Filter' => __DIR__ . '/../..' . '/app/Widgets/Filter.php',
        'Rtcl\\Widgets\\Listings' => __DIR__ . '/../..' . '/app/Widgets/Listings.php',
        'Rtcl\\Widgets\\Search' => __DIR__ . '/../..' . '/app/Widgets/Search.php',
        'Rtcl\\Widgets\\Widget' => __DIR__ . '/../..' . '/app/Widgets/Widget.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInite69e37bc7faa6512b5c83ed06d5f4b5c::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInite69e37bc7faa6512b5c83ed06d5f4b5c::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInite69e37bc7faa6512b5c83ed06d5f4b5c::$classMap;

        }, null, ClassLoader::class);
    }
}
