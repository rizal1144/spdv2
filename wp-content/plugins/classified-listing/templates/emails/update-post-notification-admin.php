<?php
/**
 * Admin Notification at edit/update post
 *
 * @package ClassifiedListing/Templates/Emails
 * @version 1.2.17
 */


if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
?>

<p>Hi Admin,</p>
<p><?php printf(__("<a href='%s'>%s</a> has updated to your site %s.", "classified-listing"), get_permalink($post), get_the_title($post), $blogname) ?></p>
<p><?php esc_html_e('Thanks for reading.', 'classified-listing'); ?></p>
