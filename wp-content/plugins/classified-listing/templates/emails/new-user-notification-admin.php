<?php
/**
 * Admin Notification at create new user
 *
 * @package ClassifiedListing/Templates/Emails
 * @version 1.2.17
 */


if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
?>

<p>Hi Admin,</p>
<p><?php printf("%s ( %s ) has registered to your site %s.", $user->user_login, $user->user_email, $blogname) ?></p>
<p><?php esc_html_e('Thanks for reading.', 'classified-listing'); ?></p>
