<?php
/**
 * User Reset Password email
 *
 * @package ClassifiedListing/Templates/Emails
 * @version 1.2.15
 */

use Rtcl\Helpers\Link;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
?>

<p><?php printf(esc_html__('Hi %s,', 'classified-listing'), esc_html($user->user_login)); ?></p>
<p><?php printf(esc_html__('Someone has requested a new password for the following account on %s:', 'classified-listing'), $blogname); ?></p>
<p><?php printf(esc_html__('Username: %s', 'classified-listing'), esc_html($user->user_login)); ?></p>
<p><?php esc_html_e('If you didn\'t make this request, just ignore this email. If you\'d like to proceed:', 'classified-listing'); ?></p>
<p>
    <a class="link" href="<?php echo esc_url(add_query_arg(array(
        'key'   => $reset_key,
        'login' => $user->user_login
    ), Link::get_my_account_page_link('lost-password'))); ?>"><?php // phpcs:ignore ?>
        <?php esc_html_e('Click here to reset your password', 'classified-listing'); ?>
    </a>
</p>
<p><?php esc_html_e('Thanks for reading.', 'classified-listing'); ?></p>
