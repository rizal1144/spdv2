<?php

use Rtcl\Helpers\Link;

?>

<div class="rtcl rtcl-search rtcl-search-inline">
    <form action="<?php echo Link::get_listings_page_link(true); ?>" data-action="<?php echo Link::get_listings_page_link(); ?>"  class="form-vertical rtcl-search-form-inline">

		<?php if ( isset( $_GET['lang'] ) ) : ?>
            <input type="hidden" name="lang" value="<?php echo esc_attr( $_GET['lang'] ); ?>">
		<?php endif; ?>

        <div class="row rtcl-no-margin">

			<?php if ( $can_search_by_location ) : ?>
                <!-- Location field -->
                <div class="form-group col-md-<?php echo esc_attr($span_top); ?>">
					<?php
					wp_dropdown_categories( array(
						'show_option_none' => '-- ' . __( 'Select a location', 'classified-listing' ) . ' --',
						'taxonomy'         => rtcl()->location,
						'name'             => 'rtcl_location',
						'id'               => 'rtcl-location-search-' . $id,
						'class'            => 'form-control rtcl-location-search',
						'selected'         => get_query_var('rtcl_location'),
						'hierarchical'     => true,
						'value_field'      => 'slug',
						'depth'            => 3,
						'show_count'       => false,
						'hide_empty'       => false,
					) );
					?>
                </div>
			<?php endif; ?>

			<?php if ( $can_search_by_category ) : ?>
                <!-- Category field -->
                <div class="form-group col-md-<?php echo esc_attr($span_top); ?>">
					<?php
					wp_dropdown_categories( array(
						'show_option_none' => '-- ' . __( 'Select a category', 'classified-listing' ) . ' --',
						'taxonomy'         => rtcl()->category,
						'name'             => 'rtcl_category',
						'id'               => 'rtcl-category-search-' . $id,
						'class'            => 'form-control rtcl-category-search',
						'selected'         => get_query_var('rtcl_category'),
						'hierarchical'     => true,
						'value_field'      => 'slug',
						'depth'            => 2,
						'show_count'       => false,
						'hide_empty'       => false,
					) );
					?>
                </div>
			<?php endif; ?>

            <div class="form-group col-md-<?php echo esc_attr($span_top + 1); ?>">
                <input type="text" name="q" class="form-control"
                       placeholder="<?php _e( 'Enter your keyword here ...', 'classified-listing' ); ?>"
                       value="<?php if ( isset( $_GET['q'] ) ) {
                           echo esc_attr( $_GET['q'] );
                       } ?>">
            </div>

            <?php if ( ! $can_search_by_price ): ?>
                <div class="form-group col-md-<?php echo esc_attr($span_top - 1); ?>">
                    <div class="rtcl-action-buttons">
                        <button type="submit"
                                class="btn btn-primary"><?php _e( 'Search', 'classified-listing' ); ?></button>
                    </div>
                </div>
            <?php endif; ?>

        </div>

		<?php if ( $can_search_by_price ) : ?>
            <div class="row rtcl-no-margin">
                <!-- Price fields -->
                <div class="form-group col-md-<?php echo esc_attr($span_bottom); ?>">
                    <label><?php _e( 'Price Range', 'classified-listing' ); ?></label>
                    <div class="row">
                        <div class="col-md-6 col-xs-6">
                            <input type="text" name="price[min]" class="form-control"
                                   placeholder="<?php _e( 'min', 'classified-listing' ); ?>"
                                   value="<?php if ( isset( $_GET['price'] ) ) {
								       echo esc_attr( $_GET['price']['min'] );
							       } ?>">
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <input type="text" name="price[max]" class="form-control"
                                   placeholder="<?php _e( 'max', 'classified-listing' ); ?>"
                                   value="<?php if ( isset( $_GET['price'] ) ) {
								       echo esc_attr( $_GET['price']['max'] );
							       } ?>">
                        </div>
                    </div>
                </div>

                <!-- Action buttons -->
                <div class="form-group col-md-<?php echo esc_attr($span_bottom); ?>">
                    <label class="hidden-sm hidden-xs">&nbsp;</label>
                    <div class="form-group col-md-<?php echo esc_attr($span_bottom); ?>">
                        <div class="rtcl-action-buttons text-right">
                            <button type="submit"
                                    class="btn btn-primary"><?php _e( 'Search', 'classified-listing' ); ?></button>
                        </div>
                    </div>

                </div>
            </div>
		<?php endif; ?>
    </form>
</div>