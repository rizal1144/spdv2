<?php

use Rtcl\Helpers\Functions;

?>
<div class="panel-block">
    <form class="rtcl-filter-form" method="GET">
        <div class="ui-accordion">
			<?php Functions::print_html( $category_filter, true ); ?>
			<?php Functions::print_html( $location_filter, true ); ?>
			<?php Functions::print_html( $ad_type_filter, true ); ?>
			<?php Functions::print_html( $price_filter, true ); ?>
        </div>
    </form>
</div>
