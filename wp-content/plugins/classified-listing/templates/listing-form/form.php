<?php
/**
 * Login Form
 *
 * @author 		RadiusTheme
 * @package 	classified-listing/templates
 * @version     1.0.0
 */

?>

<div class="rtcl rtcl-user rtcl-post-form-wrap">
    <form action="" method="post" id="rtcl-post-form" class="form-vertical">
        <div class="rtcl-post">
            <?php do_action("rtcl_listing_form", $post_id); ?>
        </div>
		<?php wp_nonce_field( rtcl()->nonceText, rtcl()->nonceId ); ?>
        <input type="hidden" name="_post_id" id="_post_id" value="<?php echo absint($post_id); ?>"/>
        <input type="submit" class="btn btn-primary rtcl-submit-btn"
               value="<?php
               if($post_id > 0){
	               _e( 'Update', 'classified-listing' );
               }else{
	               _e( 'Submit', 'classified-listing' );
               }
               ?>"/>
    </form>
    <!-- Display response -->
    <div class="rtcl-response"></div>
</div>