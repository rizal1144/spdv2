<?php
/**
 * Terms and conditions
 *
 * @author     RadiusTheme
 * @package    classified-listing/templates
 * @version    1.1.2
 */

use Rtcl\Helpers\Functions;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

?>

<div class="rtcl-listing-terms-conditions rtcl-post-section">
    <div class="form-group">
        <div class="form-check">
            <input type="checkbox"
                   class="form-check-input"
                   name="rtcl_agree"
                   id="rtcl-terms-conditions"
                   required
                <?php echo esc_attr($rtcl_agree ? 'checked' : ''); ?>
            >
            <label class="form-check-label" for="rtcl-terms-conditions">
                <?php Functions::print_html($terms_conditions); ?>
            </label>
            <div class="with-errors help-block" data-error="<?php echo __("This field is required", 'classified-listing') ?>"></div>
        </div>
    </div>
</div>