<?php
/**
 *
 * @author        RadiusTheme
 * @package    classified-listing/templates
 * @version     1.0.0
 */


use Rtcl\Helpers\Link;
use Rtcl\Models\Listing;

global $post;
$listing = new Listing($post->id);

if (!$listing->can_show_date() && !$listing->can_show_user() && !$listing->can_show_category() && !$listing->can_show_location() && !$listing->can_show_views()) {
    return;
}
?>

<div class="rtcl-listing-meta-data">
    <?php if ($listing->can_show_date()): ?>
        <span class="updated"><span
                    class="rtcl-icon rtcl-icon-clock"></span>&nbsp;<?php $listing->the_time(); ?></span>
    <?php endif; ?>
    <?php if ($listing->can_show_user()): ?>
        <span class="author">
			<?php esc_html_e('by ', 'classified-listing'); ?>
            <?php $listing->the_author(); ?>
            </span>
    <?php endif; ?>
    <?php if ($listing->has_category() && $listing->can_show_category()):
        $category = $listing->get_categories();
        $category = end($category);
        ?><span class="rtcl-icon rtcl-icon-tags"></span>&nbsp;<?php echo esc_html($category->name) ?>
    <?php endif; ?>
    <?php if ($listing->has_location() && $listing->can_show_location()):
        $location = $listing->get_locations();
        $location = end($location);
        ?>
        <span class="rtcl-icon rtcl-icon-location"></span>&nbsp;<?php echo esc_html($location->name) ?>
    <?php endif; ?>
    <?php if ($listing->can_show_views()): ?>
        <span class="rtcl-icon rtcl-icon-eye"></span>
        <?php echo sprintf(_n("%s view", "%s views", $listing->get_view_counts(), 'classified-listing'), number_format_i18n($listing->get_view_counts())); ?>
    <?php endif; ?>
</div>
