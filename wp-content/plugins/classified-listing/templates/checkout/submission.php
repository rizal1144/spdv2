<?php
/**
 *
 * @author        RadiusTheme
 * @package    classified-listing/templates
 * @version     1.0.0
 */


use Rtcl\Helpers\Functions;
use Rtcl\Helpers\Link;

?>
<div id="rtcl-checkout">
    <p><?php _e( 'Please review your order, and click Purchase once you are ready to proceed.',
			'classified-listing' ); ?></p>

    <form id="rtcl-checkout-form" class="form-vertical rtcl-submission-form">
		<?php do_action( 'rtcl_before_submission_form' ); ?>
        <table id="rtcl-checkout-form-data" class="table table-hover table-stripped table-bordered">
            <tr>
                <th><?php _e( "Payment Option", "classified-listing" ); ?></th>
                <th><?php _e( "Description", "classified-listing" ); ?></th>
                <th><?php _e( "Visibility", "classified-listing" ); ?></th>
                <th><?php printf( __( 'Price [%s %s]', 'classified-listing' ),
						Functions::get_currency( true ),
						Functions::get_currency_symbol( null, true ) ); ?></th>
            </tr>
			<?php foreach ( $payment_options as $option ) :
				$price = get_post_meta( $option->ID, 'price', true );
				$visible = get_post_meta( $option->ID, 'visible', true );
				$featured = get_post_meta( $option->ID, 'featured', true );
				$top = get_post_meta( $option->ID, '_top', true );
				$description = get_post_meta( $option->ID, 'description', true );
				?>
                <tr>
                    <td>
						<?php
						printf( '<label><input type="radio" name="%s" value="%s" class="rtcl-checkout-payment-option" required data-price="%s"/> %s</label>',
							'payment_option_id', $option->ID, $price, $option->post_title );
						?>
                    </td>
                    <td>
						<?php echo esc_html( $description ); ?>
                    </td>
                    <td>
						<?php
						printf( '%d %s %s',
							absint( $visible ),
							__( 'Days', 'classified-listing' ),
							$featured ? '<span class="badge badge-info">' . __( 'Featured', 'classified-listing' ) . '</span>' : null
						);
						?>
                    </td>
                    <td align="right"
                        class="text-right"><?php echo Functions::get_formatted_amount( $price, true ); ?> </td>
                </tr>
			<?php endforeach; ?>
        </table>

        <div id="rtcl-payment-methods">
            <h4 class="pm-heading"><?php _e( 'Choose payment method', 'classified-listing' ); ?></h4>

			<?php echo Functions::get_payment_method_list(); ?>
        </div>
        <p id="rtcl-checkout-errors" class="text-danger"></p>
		<?php wp_nonce_field( 'rtcl_checkout', 'rtcl_checkout_nonce' ); ?>
        <input type="hidden" name="post_id" value="<?php echo absint($post_id); ?>"/>
        <input type="hidden" name="action" value="rtcl_ajax_checkout_action"/>
        <div class="text-right">
            <a href="<?php echo Link::get_account_endpoint_url( "listings" ); ?>"
               class="btn btn-info"><?php _e( 'Go to Manage Listing',
					'classified-listing' ); ?></a>
            <input type="submit" id="rtcl-checkout-submit-btn" name="rtcl-checkout" class="btn btn-primary submission-btn"
                   value="<?php _e( 'Proceed to payment', 'classified-listing' ); ?>"/>
        </div>
		<?php do_action( 'rtcl_before_submission_form' ); ?>
    </form>
</div>