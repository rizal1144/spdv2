<?php
/**
 *
 * @author        RadiusTheme
 * @package    classified-listing/templates
 * @version     1.0.0
 */


use Rtcl\Helpers\Functions;
use Rtcl\Helpers\Link;


Functions::print_notices();
?>

<div class="rtcl-payment-receipt">
	<?php
	if ( $Payment->Gateway->id === "offline" && $Payment->get_status() === "rtcl-pending" ) {
		Functions::the_offline_payment_instructions();
	}
	$data = array();
	ob_start();
	?>
    <div class="payment-info">
        <div class="row">
            <div class="col-md-6">
                <table class="table table-bordered">
                    <tr>
                        <td><?php _e( 'PAYMENT', 'classified-listing' ); ?> #</td>
                        <td><?php echo absint($Payment->get_id()); ?></td>
                    </tr>

                    <tr>
                        <td><?php _e( 'Total Amount', 'classified-listing' ); ?></td>
                        <td>
							<?php
							if ( $amount = $Payment->get_total() ) {
								echo Functions::get_formatted_price( $amount, true );
							}
							?>
                        </td>
                    </tr>

                    <tr>
                        <td><?php _e( 'Date', 'classified-listing' ); ?></td>
                        <td>
							<?php echo Functions::datetime( 'rtcl', $Payment->get_date_paid() ); ?>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-md-6">
                <table class="table table-bordered">
                    <tr>
                        <td><?php _e( 'Payment Method', 'classified-listing' ); ?></td>
                        <td>
							<?php
							$gateway = Functions::get_payment_gateway( $Payment->get_payment_method() );
							if ( is_object( $gateway ) ) {
								echo esc_html($gateway->get_title());
							}
							?>
                        </td>
                    </tr>

                    <tr>
                        <td><?php _e( 'Payment Status', 'classified-listing' ); ?></td>
                        <td>
							<?php
							echo Functions::get_status_i18n( $Payment->get_status() );
							?>
                        </td>
                    </tr

                    >
                    <tr>
                        <td><?php _e( 'Transaction Key', 'classified-listing' ); ?></td>
                        <td><?php echo esc_html($Payment->get_transaction_id()); ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
	<?php
	$data['payment_info'] = ob_get_clean();
	ob_start();
	?>
    <div class="pricing-info">
        <h2><?php _e( 'Details', 'classified-listing' ); ?></h2>
        <table class="table table-bordered table-striped">
            <tr>
                <th colspan="2"><?php echo get_the_title( $Payment->get_listing_id() ); ?></th>
            </tr>
            <tr>
                <td class="text-right rtcl-vertical-middle"><?php _e( 'Payment Option ', 'classified-listing' ); ?></td>
                <td><?php echo esc_html($Payment->Option->getTitle()); ?></td>
            </tr>
            <tr>
                <td class="text-right rtcl-vertical-middle"><?php _e( 'Duration ', 'classified-listing' ); ?></td>
                <td><?php
					printf( '%d %s%s',
						absint( $Payment->Option->getVisible() ),
						__( 'Days', 'classified-listing' ),
						$Payment->Option->getFeatured() ? '<span class="badge badge-info">' . __( 'Featured', 'classified-listing' ) . '</span>' : null
					); ?></td>
            </tr>
            <tr>
                <td class="text-right rtcl-vertical-middle"><?php _e( 'Amount ', 'classified-listing' ); ?></td>
                <td><?php echo Functions::get_formatted_price( $Payment->Option->getPrice(), true ); ?></td>
            </tr>
        </table>
    </div>
	<?php
	$data['pricing_info'] = ob_get_clean();
	ob_start();
	?>
    <div class="action-btn text-center">
        <a href="<?php echo Link::get_my_account_page_link( "listings" ); ?>"
           class="btn btn-success"><?php _e( 'View all my listings', 'classified-listing' ); ?></a>
    </div>
	<?php
	$data['action_btn'] = ob_get_clean();
	$data               = apply_filters( 'rtcl_payment_receipt_html', $data, $Payment );

	echo implode( '', $data );
	?>
</div>