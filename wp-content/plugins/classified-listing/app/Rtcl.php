<?php

require_once __DIR__ . './../vendor/autoload.php';

use Rtcl\Controllers\Ajax\Ajax;
use Rtcl\Controllers\Hooks\Actions;
use Rtcl\Controllers\Hooks\AdminHooks;
use Rtcl\Controllers\Hooks\FrontEndHooks;
use Rtcl\Controllers\Install;
use Rtcl\Controllers\PublicAction;
use Rtcl\Controllers\Admin\AdminController;
use Rtcl\Controllers\Query;
use Rtcl\Controllers\SessionHandler;
use Rtcl\Helpers\Functions;
use Rtcl\Models\PaymentGateways;
use Rtcl\Widgets\Widget;

/**
 * Class Rtcl
 */
final class Rtcl
{


    /**
     * Query instance.
     *
     * @var Query
     */
    public $query = null;

    protected static $instance = null;
    public $post_type = "rtcl_listing";
    public $post_type_cfg = "rtcl_cfg";
    public $post_type_cf = "rtcl_cf";
    public $post_type_payment = "rtcl_payment";
    public $post_type_pricing = "rtcl_pricing";
    public $category = "rtcl_category";
    public $location = "rtcl_location";
    public $nonceId = "__rtcl_wpnonce";
    public $nonceText = "rtcl_nonce_secret";
    private $listing_types_option = "rtcl_listing_types";
    public $api = 'rtcl/v1';
    public $gallery = array();
    public $session = null;
    public $upload_directory = "classified-listing";

    /**
     * @return null|Rtcl
     */
    public static function get_instance() {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }


    /**
     * Cloning is forbidden.
     *
     * @since 1.0
     */
    public function __clone() {
        Functions::doing_it_wrong(__FUNCTION__, __('Cheatin&#8217; huh?', 'classified-listing'), '1.0');
    }

    /**
     * Unserializing instances of this class is forbidden.
     *
     * @since 1.0
     */
    public function __wakeup() {
        Functions::doing_it_wrong(__FUNCTION__, __('Cheatin&#8217; huh?', 'classified-listing'), '1.0');
    }

    /**
     * Auto-load in-accessible properties on demand.
     *
     * @param mixed $key Key name.
     *
     * @return mixed
     */
    public function __get($key) {
        if (in_array($key, array('init', 'payment_gateways', 'plugins_loaded'), true)) {
            return $this->$key();
        }
    }


    /**
     * Rtcl Constructor.
     */
    public function plugins_loaded() {
        $this->define_constants();
        // Add front end hook
        if (!is_admin()) {
            FrontEndHooks::init();
        }
        // Add Admin hook
        if (is_admin()) {
            AdminHooks::init();
        }
        // add hook for both
        Actions::init();

        $this->query = new Query();
        $this->init_hooks();
        do_action('rtcl_loaded');
    }


    private function init_hooks() {
        // Do action
        do_action('rtcl_before_init');
        $this->load_plugin_textdomain();
        $this->load_session();
        new AdminController();
        new Ajax();
        new PublicAction();
        new Widget();
        do_action('rtcl_init');
    }

    private function load_session() {
        $this->session = new SessionHandler();
        $this->session->init();
    }


    /**
     * Load Localisation files.
     *
     * Note: the first-loaded translation file overrides any following ones if the same translation is present.
     *
     * Locales found in:
     *      - WP_LANG_DIR/classified-listing/classified-listing-LOCALE.mo
     *      - WP_LANG_DIR/plugins/classified-listing-LOCALE.mo
     */
    public function load_plugin_textdomain() {
        $locale = is_admin() && function_exists('get_user_locale') ? get_user_locale() : get_locale();
        $locale = apply_filters('plugin_locale', $locale, 'classified-listing');
        unload_textdomain('classified-listing');
        load_textdomain('classified-listing', WP_LANG_DIR . '/classified-listing/classified-listing-' . $locale . '.mo');
        load_plugin_textdomain('classified-listing', false, plugin_basename(dirname(RTCL_PLUGIN_FILE)) . '/languages');
    }


    /**
     * Get gateways class.
     *
     * @return array
     */
    public function payment_gateways() {
        return PaymentGateways::instance()->payment_gateways;
    }


    private function define_constants() {
        $this->define('RTCL_PATH', plugin_dir_path(RTCL_PLUGIN_FILE));
        $this->define('RTCL_URL', plugins_url('', RTCL_PLUGIN_FILE));
        $this->define('RTCL_SLUG', basename(dirname(RTCL_PLUGIN_FILE)));
        $this->define('RTCL_SESSION_CACHE_GROUP', 'rtcl_session_id');

    }

    /**
     * Define constant if not already set.
     *
     * @param string      $name  Constant name.
     * @param string|bool $value Constant value.
     */
    private function define($name, $value) {
        if (!defined($name)) {
            define($name, $value);
        }
    }

    public function version() {
        return RTCL_VERSION;
    }


    public function get_listing_types_option_id() {
        return $this->listing_types_option;
    }

    public function get_assets_uri($file) {
        $file = ltrim($file, '/');

        return trailingslashit(RTCL_URL . '/assets') . $file;
    }

}

/**
 * @return null|Rtcl
 */
function rtcl() {
    return Rtcl::get_instance();
}

register_activation_hook(RTCL_PLUGIN_FILE, array(Install::class, 'activate'));
register_deactivation_hook(RTCL_PLUGIN_FILE, array(Install::class, 'deactivate'));

add_action('plugins_loaded', array(rtcl(), 'plugins_loaded'));