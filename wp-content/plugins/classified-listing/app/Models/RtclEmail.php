<?php

namespace Rtcl\Models;


use Exception;
use Rtcl\Controllers\Admin\EmailSettings;
use Rtcl\Helpers\Functions;
use Rtcl\Helpers\Link;

class RtclEmail
{

    protected $type;
    protected $data;
    protected $to;
    protected $body;
    protected $email_settings;
    protected $placeholders;
    private $cc = [];
    private $bcc = [];
    private $from;
    private $subject = '';
    private $headers = [];
    private $attachments = [];

    private $variables = [];
    private $template = false;
    private $sendAsHTML = true;

    /**
     * RtclEmail constructor.
     *
     * @param string $type
     */
    public function __construct($type = 'default') {
        $this->type = $type;
        $this->setEmailSettings();
    }


    /**
     * Set recipients
     *
     * @param Array|String $to
     *
     * @return Object $this
     */
    public function to($to) {
        if (is_array($to)) {
            $this->to = $to;
        } else {
            $this->to = [$to];
        }

        return $this;
    }

    /**
     * Get recipients
     *
     * @return Array $to
     */
    public function getTo() {
        return $this->to;
    }


    /**
     * Set Cc recipients
     *
     * @param String|Array $cc
     *
     * @return Object $this
     */
    public function cc($cc) {
        if (is_array($cc)) {
            $this->cc = $cc;
        } else {
            $this->cc = [$cc];
        }

        return $this;
    }


    /**
     * Get Cc recipients
     *
     * @return Array $cc
     */
    public function getCc() {
        return $this->cc;
    }


    /**
     * Set Email Bcc recipients
     *
     * @param String|Array $bcc
     *
     * @return Object $this
     */
    public function bcc($bcc) {
        if (is_array($bcc)) {
            $this->bcc = $bcc;
        } else {
            $this->bcc = [$bcc];
        }

        return $this;
    }


    /**
     * Set email Bcc recipients
     *
     * @return array $bcc
     */
    public function getBcc() {
        return $this->bcc;
    }

    /**
     * Set email Subject
     *
     * @param Srting $subject
     *
     * @return Object $this
     */
    public function subject($subject) {
        $this->subject = $subject;

        return $this;
    }


    /**
     * Return email subject
     *
     * @return string
     */
    public function getSubject() {
        return $this->subject;
    }

    /**
     * Set From header
     *
     * @param String
     *
     * @return Object $this
     */
    public function from($from) {
        $this->from = $from;

        return $this;
    }

    /**
     * Set the email's headers
     *
     * @param String|array $headers [description]
     *
     * @return Object $this
     */
    public function headers($headers) {
        if (is_array($headers)) {
            $this->headers = $headers;
        } else {
            $this->headers = [$headers];
        }

        return $this;
    }


    /**
     * Return headers
     *
     * @return array
     */
    public function getHeaders() {
        return $this->headers;
    }


    /**
     * Returns email content type
     *
     * @return String
     */
    public static function HTMLFilter() {
        return 'text/html';
    }


    /**
     * Set email content type
     *
     * @param Bool $html
     *
     * @return Object $this
     */
    public function sendAsHTML($html) {
        $this->sendAsHTML = $html;

        return $this;
    }


    /**
     * Attach a file or array of files.
     * File paths must be absolute.
     *
     * @param String|array $path
     *
     * @return Object $this
     * @throws \Exception
     */
    public function attach($path) {
        if (is_array($path)) {
            $this->attachments = [];
            foreach ($path as $path_) {
                if (!file_exists($path_)) {
                    throw new Exception("Attachment not found at $path");
                } else {
                    $this->attachments[] = $path_;
                }
            }
        } else {
            if (!file_exists($path)) {
                throw new Exception("Attachment not found at $path");
            }
            $this->attachments = [$path];
        }

        return $this;
    }

    /**
     * Set the template file
     *
     * @param string $template Path to HTML template
     * @param array  $variables
     *
     * @return Object $this
     */
    public function template($template, $variables = []) {

        if (is_array($variables)) {
            $this->variables = $variables;
        }

        $this->template = $template;

        return $this;
    }

    /**
     * Renders the template
     *
     * @return string
     */
    private function render() {
        return Functions::get_template_html($this->template, $this->variables);
    }

    /**
     * @param array $data
     */
    public function setData($data) {

        switch ($this->type) {

            case 'listing_contact':
                // vars
                $post_author_id = get_post_field('post_author', $data['post_id']);
                $user = get_userdata($post_author_id);
                $listing_title = get_the_title($data['post_id']);
                $listing_url = get_permalink($data['post_id']);
                $placeholders = wp_parse_args(array(
                    '{name}'          => $user->display_name,
                    '{username}'      => $user->user_login,
                    '{listing_title}' => $listing_title,
                    '{listing_link}'  => sprintf('<a href="%s">%s</a>', $listing_url, $listing_title),
                    '{listing_url}'   => sprintf('<a href="%s">%s</a>', $listing_url, $listing_url),
                    '{sender_name}'   => $data['name'],
                    '{sender_email}'  => $data['email'],
                    '{message}'       => $data['message'],
                ), $this->placeholders);
                $to = $user->user_email;
                $subject = !empty($this->email_settings['contact_subject']) ? $this->email_settings['contact_subject'] : null;
                $body = !empty($this->email_settings['contact_body']) ? $this->email_settings['contact_body'] : null;
                $subject = strtr($subject, $placeholders);
                $message = strtr($body, $placeholders);
                $message = nl2br($message);

                $headers = "From: {$data['name']} <{$data['email']}>\r\n";
                $headers .= "Reply-To: {$data['email']}\r\n";

                $this->to = $to;
                $this->subject = $subject;
                $this->body = $message;
                $this->setHeaders($headers);

                break;

            case 'listing_contact_admin':
                // vars
                $post_author_id = get_post_field('post_author', $data['post_id']);
                $user = get_userdata($post_author_id);
                $listing_title = get_the_title($data['post_id']);
                $listing_url = get_permalink($data['post_id']);
                $placeholders = wp_parse_args(array(
                    '{name}'          => $user->display_name,
                    '{username}'      => $user->user_login,
                    '{listing_title}' => $listing_title,
                    '{listing_link}'  => sprintf('<a href="%s">%s</a>', $listing_url, $listing_title),
                    '{listing_url}'   => sprintf('<a href="%s">%s</a>', $listing_url, $listing_url),
                    '{sender_name}'   => $data['name'],
                    '{sender_email}'  => $data['email'],
                    '{message}'       => $data['message'],
                ), $this->placeholders);

                $to = Functions::get_admin_email_id_s();

                $subject = __('[{site_name}] Contact via "{listing_title}"', 'classified-listing');
                $subject = strtr($subject, $placeholders);

                $message = __("Dear Administrator,<br /><br />A listing on your website {site_name} received a message.<br /><br />Listing URL: {listing_url}<br /><br />Name: {sender_name}<br />Email: {sender_email}<br />Message: {message}<br />Time: {now}<br /><br />This is just a copy of the original email and was already sent to the listing owner. You don't have to reply this unless necessary.", 'classified-listing');
                $message = strtr($message, $placeholders);

                $headers = "From: {$data['name']} <{$data['email']}>\r\n";
                $headers .= "Reply-To: {$data['email']}\r\n";

                $this->to = $to;
                $this->subject = $subject;
                $this->body = $message;
                $this->setHeaders($headers);

                break;

            case 'report_abuse':

                // vars
                $user = wp_get_current_user();
                $listing_title = get_the_title($data['post_id']);
                $listing_url = get_permalink($data['post_id']);

                $placeholders = wp_parse_args(array(
                    '{listing_title}' => $listing_title,
                    '{listing_link}'  => sprintf('<a href="%s">%s</a>', $listing_url, $listing_title),
                    '{listing_url}'   => sprintf('<a href="%s">%s</a>', $listing_url, $listing_url),
                    '{sender_name}'   => $user->display_name,
                    '{sender_email}'  => $user->user_email,
                    '{message}'       => $data['message'],
                ), $this->placeholders);

                $to = $this->get_admin_email_id_s();

                $subject = __('[{site_name}] Report Abuse via "{listing_title}"', 'classified-listing');
                $subject = strtr($subject, $placeholders);

                $message = __("Dear Administrator,<br /><br />This is an email abuse report for a listing at {listing_url}.<br /><br />Name: {sender_name}<br />Email: {sender_email}<br />Message: {message}<br />Time: {now}",
                    'classified-listing');
                $message = strtr($message, $placeholders);

                $headers = "From: {$user->display_name} <{$user->user_email}>\r\n";
                $headers .= "Reply-To: {$user->user_email}\r\n";

                $this->to = $to;
                $this->subject = $subject;
                $this->body = $message;
                $this->setHeaders($headers);

                break;

            case "listing_published":
                $post_author_id = get_post_field('post_author', $data['post_id']);
                $user = get_userdata($post_author_id);
                $listing_title = get_the_title($data['post_id']);
                $listing_url = get_permalink($data['post_id']);

                $placeholders = wp_parse_args(array(
                    '{name}'          => $user->display_name,
                    '{username}'      => $user->user_login,
                    '{listing_title}' => $listing_title,
                    '{listing_link}'  => sprintf('<a href="%s">%s</a>', $listing_url, $listing_title),
                    '{listing_url}'   => sprintf('<a href="%s">%s</a>', $listing_url, $listing_url)
                ), $this->placeholders);

                $to = $user->user_email;
                $subject = !empty($this->email_settings['listing_published_subject']) ? $this->email_settings['listing_published_subject'] : null;
                $body = !empty($this->email_settings['listing_published_body']) ? $this->email_settings['listing_published_body'] : null;

                $subject = strtr($subject, $placeholders);
                $message = strtr($body, $placeholders);
                $message = nl2br($message);

                $this->to = $to;
                $this->subject = $subject;
                $this->body = $message;
                $this->setHeaders();

                break;

            case "listing_submitted":

                $post_author_id = get_post_field('post_author', $data['post_id']);
                $user = get_userdata($post_author_id);
                $listing_title = get_the_title($data['post_id']);
                $listing_url = get_permalink($data['post_id']);

                $placeholders = wp_parse_args(array(
                    '{name}'          => $user->display_name,
                    '{username}'      => $user->user_login,
                    '{listing_title}' => $listing_title,
                    '{listing_link}'  => sprintf('<a href="%s">%s</a>', $listing_url, $listing_title),
                    '{listing_url}'   => sprintf('<a href="%s">%s</a>', $listing_url, $listing_url)
                ), $this->placeholders);

                $to = $user->user_email;
                $subject = !empty($this->email_settings['listing_submitted_subject']) ? $this->email_settings['listing_submitted_subject'] : null;
                $body = !empty($this->email_settings['listing_submitted_body']) ? $this->email_settings['listing_submitted_body'] : null;

                $subject = strtr($subject, $placeholders);
                $message = strtr($body, $placeholders);
                $message = nl2br($message);

                $this->to = $to;
                $this->subject = $subject;
                $this->body = $message;
                $this->setHeaders();

                break;

            case "listing_submitted_admin":

                $listing_id = absint($data['post_id']);
                $post_author_id = get_post_field('post_author', $listing_id);
                $user = get_userdata($post_author_id);
                $listing_title = get_the_title($listing_id);
                $listing_url = get_permalink($listing_id);

                $placeholders = wp_parse_args(array(
                    '{name}'           => $user->display_name,
                    '{username}'       => $user->user_login,
                    '{listing_id}'     => $listing_id,
                    '{listing_title}'  => $listing_title,
                    '{listing_status}' => Functions::get_status_i18n(get_post_status($listing_id)),
                    '{listing_link}'   => sprintf('<a href="%s">%s</a>', $listing_url, $listing_title),
                    '{listing_url}'    => sprintf('<a href="%s">%s</a>', $listing_url, $listing_url)
                ), $this->placeholders);

                $to = Functions::get_admin_email_id_s();

                $subject = __('[{site_name}] New Listing received', 'classified-listing');
                $subject = strtr($subject, $placeholders);

                $message = __("Dear Administrator,<br /><br />You have received a new listing on the website {site_name}.<br />This e-mail contains the listing details:<br /><br />Listing ID: {listing_id}<br />Listing Title: {listing_title}<br />Listing Status: {listing_status}<br /><br />Please do not respond to this message. It is automatically generated and is for information purposes only.", 'classified-listing');
                $message = strtr($message, $placeholders);

                $this->to = $to;
                $this->subject = $subject;
                $this->body = $message;
                $this->setHeaders();

                break;

            case 'moderator_notification_to_user':

                // vars
                $post_author_id = get_post_field('post_author', $data['post_id']);
                $user = get_userdata($post_author_id);
                $listing_title = get_the_title($data['post_id']);
                $listing_url = get_permalink($data['post_id']);
                $placeholders = wp_parse_args(array(
                    '{name}'          => $user->display_name,
                    '{username}'      => $user->user_login,
                    '{listing_title}' => $listing_title,
                    '{listing_link}'  => sprintf('<a href="%s">%s</a>', $listing_url, $listing_title),
                    '{listing_url}'   => sprintf('<a href="%s">%s</a>', $listing_url, $listing_url),
                    '{message}'       => $data['message']
                ), $this->placeholders);
                $to = $user->user_email;
                $subject = __('[{site_name}] Notification from Moderator for your Listing "{listing_title}"',
                    'classified-listing');
                $body = __("Dear {name},\n\nYou have received a Notification the administrator from your listing at {listing_url} to improve your listing.\n\nMessage: {message}\nTime: {now}\n\nThanks,\nThe Administrator of {site_name}",
                    'classified-listing');
                $subject = strtr($subject, $placeholders);
                $message = strtr($body, $placeholders);
                $message = nl2br($message);

                $this->to = $to;
                $this->subject = $subject;
                $this->body = $message;
                $this->setHeaders();

                break;

            case 'listing_renewal':

                // vars
                $post_author_id = get_post_field('post_author', $data['post_id']);
                $user = get_userdata($post_author_id);
                $never_expires = get_post_meta($data['post_id'], 'never_expires', true);
                $expiry_date = get_post_meta($data['post_id'], 'expiry_date', true);
                $categories = wp_get_object_terms($data['post_id'], rtcl()->category, array('fields' => 'names'));
                $listing_title = get_the_title($data['post_id']);
                $listing_url = get_permalink($data['post_id']);

                $placeholders = wp_parse_args(array(
                    '{name}'            => $user->display_name,
                    '{username}'        => $user->user_login,
                    '{expiration_date}' => !empty($never_expires) ? __('Never Expires', 'classified-listing') : date_i18n(get_option('date_format'), strtotime($expiry_date)),
                    '{category_name}'   => !empty($categories) ? $categories[0] : '',
                    '{renewal_link}'    => Link::get_listing_promote_page_link($data['post_id']),
                    '{listing_title}'   => $listing_title,
                    '{listing_link}'    => sprintf('<a href="%s">%s</a>', $listing_url, $listing_title),
                    '{listing_url}'     => sprintf('<a href="%s">%s</a>', $listing_url, $listing_url),
                ), $this->placeholders);

                $to = $user->user_email;
                $subject = !empty($this->email_settings['renewal_subject']) ? $this->email_settings['renewal_subject'] : null;
                $body = !empty($this->email_settings['renewal_body']) ? $this->email_settings['renewal_body'] : null;

                $subject = strtr($subject, $placeholders);
                $message = strtr($body, $placeholders);
                $message = nl2br($message);

                $this->to = $to;
                $this->subject = $subject;
                $this->body = $message;
                $this->setHeaders();

                break;

            case 'listing_expired':

                // vars
                $post_author_id = get_post_field('post_author', $data['post_id']);
                $user = get_userdata($post_author_id);
                $never_expires = get_post_meta($data['post_id'], 'never_expires', true);
                $expiry_date = get_post_meta($data['post_id'], 'expiry_date', true);
                $categories = wp_get_object_terms($data['post_id'], rtcl()->category, array('fields' => 'names'));
                $listing_title = get_the_title($data['post_id']);
                $listing_url = get_permalink($data['post_id']);

                $placeholders = wp_parse_args(array(
                    '{name}'            => $user->display_name,
                    '{username}'        => $user->user_login,
                    '{expiration_date}' => !empty($never_expires) ? __('Never Expires', 'classified-listing') : date_i18n(get_option('date_format'), strtotime($expiry_date)),
                    '{category_name}'   => !empty($categories) ? $categories[0] : '',
                    '{renewal_link}'    => Link::get_listing_promote_page_link($data['post_id']),
                    '{listing_title}'   => $listing_title,
                    '{listing_link}'    => sprintf('<a href="%s">%s</a>', $listing_url, $listing_title),
                    '{listing_url}'     => sprintf('<a href="%s">%s</a>', $listing_url, $listing_url)
                ), $this->placeholders);

                $to = $user->user_email;
                $subject = !empty($this->email_settings['expired_subject']) ? $this->email_settings['expired_subject'] : null;
                $body = !empty($this->email_settings['expired_body']) ? $this->email_settings['expired_body'] : null;

                $subject = strtr($subject, $placeholders);
                $message = strtr($body, $placeholders);
                $message = nl2br($message);

                $this->to = $to;
                $this->subject = $subject;
                $this->body = $message;
                $this->setHeaders();

                break;

            case 'listing_expired_admin':

                // vars
                $post_author_id = get_post_field('post_author', $data['post_id']);
                $user = get_userdata($post_author_id);
                $never_expires = get_post_meta($data['post_id'], 'never_expires', true);
                $expiry_date = get_post_meta($data['post_id'], 'expiry_date', true);
                $categories = wp_get_object_terms($data['post_id'], rtcl()->category, array('fields' => 'names'));
                $listing_title = get_the_title($data['post_id']);
                $listing_url = get_permalink($data['post_id']);
                $placeholders = wp_parse_args(array(
                    '{name}'            => $user->display_name,
                    '{username}'        => $user->user_login,
                    '{expiration_date}' => !empty($never_expires) ? __('Never Expires', 'classified-listing') : date_i18n(get_option('date_format'), strtotime($expiry_date)),
                    '{category_name}'   => !empty($categories) ? $categories[0] : '',
                    '{renewal_link}'    => Link::get_listing_promote_page_link($data['post_id']),
                    '{listing_title}'   => $listing_title,
                    '{listing_link}'    => sprintf('<a href="%s">%s</a>', $listing_url, $listing_title),
                    '{listing_url}'     => sprintf('<a href="%s">%s</a>', $listing_url, $listing_url)
                ), $this->placeholders);

                $to = Functions::get_admin_email_id_s();

                $subject = __('[{site_name}] Listing "{listing_title}" expired', 'classified-listing');
                $subject = strtr($subject, $placeholders);

                $message = __("Dear Administrator,<br /><br />This notification was for the listing on the website {site_name} \"{listing_title}\" and is expired.<br />This e-mail contains the listing details:<br /><br />Listing ID:{listing_id}<br />Listing Title:{listing_title}<br />Expired on:{expiration_date}<br /><br />Please do not respond to this message. It is automatically generated and is for information purposes only.", 'classified-listing');
                $message = strtr($message, $placeholders);

                $this->to = $to;
                $this->subject = $subject;
                $this->body = $message;
                $this->setHeaders();

                break;

            case 'listing_renewal_reminder':

                // vars
                $post_author_id = get_post_field('post_author', $data['post_id']);
                $user = get_userdata($post_author_id);
                $never_expires = get_post_meta($data['post_id'], 'never_expires', true);
                $expiry_date = get_post_meta($data['post_id'], 'expiry_date', true);
                $categories = wp_get_object_terms($data['post_id'], rtcl()->category, array('fields' => 'names'));
                $listing_title = get_the_title($data['post_id']);
                $listing_url = get_permalink($data['post_id']);

                $placeholders = wp_parse_args(array(
                    '{name}'            => $user->display_name,
                    '{username}'        => $user->user_login,
                    '{expiration_date}' => !empty($never_expires) ? __('Never Expires', 'classified-listing') : date_i18n(get_option('date_format'), strtotime($expiry_date)),
                    '{category_name}'   => !empty($categories) ? $categories[0] : '',
                    '{renewal_link}'    => Link::get_listing_promote_page_link($data['post_id']),
                    '{listing_title}'   => $listing_title,
                    '{listing_link}'    => sprintf('<a href="%s">%s</a>', $listing_url, $listing_title),
                    '{listing_url}'     => sprintf('<a href="%s">%s</a>', $listing_url, $listing_url),
                ), $this->placeholders);

                $to = $user->user_email;
                $subject = !empty($this->email_settings['renewal_reminder_subject']) ? $this->email_settings['renewal_reminder_subject'] : null;
                $body = !empty($this->email_settings['renewal_reminder_body']) ? $this->email_settings['renewal_reminder_body'] : null;

                $subject = strtr($subject, $placeholders);
                $message = strtr($body, $placeholders);
                $message = nl2br($message);

                $this->to = $to;
                $this->subject = $subject;
                $this->body = $message;
                $this->setHeaders();

                break;

            case 'order_created':
                $post_id = get_post_meta($data['payment_id'], 'listing_id', true);
                $post_author_id = get_post_field('post_author', $post_id);
                $user = get_userdata($post_author_id);
                $listing_title = get_the_title($post_id);
                $listing_url = get_permalink($post_id);
                $payment = new Payment($data['payment_id']);
                $placeholders = wp_parse_args(array(
                    '{name}'          => $user->display_name,
                    '{username}'      => $user->user_login,
                    '{listing_title}' => $listing_title,
                    '{listing_link}'  => sprintf('<a href="%s">%s</a>', $listing_url, $listing_title),
                    '{listing_url}'   => sprintf('<a href="%s">%s</a>', $listing_url, $listing_url),
                    '{order_id}'      => $data['payment_id'],
                    '{order_page}'    => Link::get_payment_receipt_page_link($data['payment_id']),
                    '{order_details}' => $payment->get_details()
                ), $this->placeholders);

                $to = $user->user_email;
                $subject = !empty($this->email_settings['order_created_subject']) ? $this->email_settings['order_created_subject'] : null;
                $body = !empty($this->email_settings['order_created_body']) ? $this->email_settings['order_created_body'] : null;

                $subject = strtr($subject, $placeholders);
                $message = strtr($body, $placeholders);
                $message = nl2br($message);

                $this->to = $to;
                $this->subject = $subject;
                $this->body = $message;
                $this->setHeaders();

                break;

            case 'order_created_admin':
                $post_id = get_post_meta($data['payment_id'], 'listing_id', true);
                $post_author_id = get_post_field('post_author', $post_id);
                $user = get_userdata($post_author_id);
                $listing_title = get_the_title($post_id);
                $listing_url = get_permalink($post_id);
                $payment = new Payment($data['payment_id']);
                $placeholders = wp_parse_args(array(
                    '{name}'          => $user->display_name,
                    '{username}'      => $user->user_login,
                    '{listing_title}' => $listing_title,
                    '{listing_link}'  => sprintf('<a href="%s">%s</a>', $listing_url, $listing_title),
                    '{listing_url}'   => sprintf('<a href="%s">%s</a>', $listing_url, $listing_url),
                    '{order_id}'      => $data['payment_id'],
                    '{order_page}'    => Link::get_payment_receipt_page_link($data['payment_id']),
                    '{order_details}' => $payment->get_details()
                ), $this->placeholders);
                $to = Functions::get_admin_email_id_s();

                $subject = __('[{site_name}] A new order has been created on your website', 'classified-listing');
                $subject = strtr($subject, $placeholders);

                $message = __("Dear Administrator,<br /><br />The order is now created.<br /><br />This notification was for the order #{order_id} on the website {site_name}.<br />You can access the order details directly by clicking on the link below after logging in your back end:<br />{order_page}<br /><br />{order_details}<br /><br />Please do not respond to this message. It is automatically generated and is for information purposes only.", 'classified-listing');
                $message = strtr($message, $placeholders);

                $this->to = $to;
                $this->subject = $subject;
                $this->body = $message;
                $this->setHeaders();

                break;

            case 'order_completed':
                $post_id = get_post_meta($data['payment_id'], 'listing_id', true);
                $post_author_id = get_post_field('post_author', $post_id);
                $user = get_userdata($post_author_id);
                $listing_title = get_the_title($post_id);
                $listing_url = get_permalink($post_id);
                $payment = new Payment($data['payment_id']);

                $placeholders = wp_parse_args(array(
                    '{name}'          => $user->display_name,
                    '{username}'      => $user->user_login,
                    '{listing_title}' => $listing_title,
                    '{listing_link}'  => sprintf('<a href="%s">%s</a>', $listing_url, $listing_title),
                    '{listing_url}'   => sprintf('<a href="%s">%s</a>', $listing_url, $listing_url),
                    '{order_id}'      => $data['payment_id'],
                    '{order_page}'    => Link::get_payment_receipt_page_link($data['payment_id']),
                    '{order_details}' => $payment->get_details()
                ), $this->placeholders);

                $to = $user->user_email;
                $subject = !empty($this->email_settings['order_completed_subject']) ? $this->email_settings['order_completed_subject'] : null;
                $body = !empty($this->email_settings['order_completed_body']) ? $this->email_settings['order_completed_body'] : null;

                $subject = strtr($subject, $placeholders);
                $message = strtr($body, $placeholders);
                $message = nl2br($message);

                $this->to = $to;
                $this->subject = $subject;
                $this->body = $message;
                $this->setHeaders();

                break;

            case 'order_completed_admin':
                $post_id = get_post_meta($data['payment_id'], 'listing_id', true);
                $post_author_id = get_post_field('post_author', $post_id);
                $user = get_userdata($post_author_id);
                $listing_title = get_the_title($post_id);
                $listing_url = get_permalink($post_id);
                $payment = new Payment($data['payment_id']);

                $placeholders = wp_parse_args(array(
                    '{name}'          => $user->display_name,
                    '{username}'      => $user->user_login,
                    '{listing_title}' => $listing_title,
                    '{listing_link}'  => sprintf('<a href="%s">%s</a>', $listing_url, $listing_title),
                    '{listing_url}'   => sprintf('<a href="%s">%s</a>', $listing_url, $listing_url),
                    '{order_id}'      => $data['payment_id'],
                    '{order_page}'    => Link::get_payment_receipt_page_link($data['payment_id']),
                    '{order_details}' => $payment->get_details()
                ), $this->placeholders);

                $to = Functions::get_admin_email_id_s();

                $subject = __('[{site_name}] Payment notification : payment Completed for order no.{order_id}', 'classified-listing');
                $subject = strtr($subject, $placeholders);

                $message = __("Dear Administrator,<br /><br />A Payment notification was received with the status Completed. The order is now confirmed.<br /><br />This notification was for the order #{order_id} on the website {site_name}.<br />You can access the order details directly by clicking on the link below after logging in your back end:<br />{order_page}<br /><br />{order_details}<br /><br />Please do not respond to this message. It is automatically generated and is for information purposes only.", 'classified-listing');
                $message = strtr($message, $placeholders);

                $this->to = $to;
                $this->subject = $subject;
                $this->body = $message;
                $this->setHeaders();

                break;

            default:

                $this->to = $data['to'];
                $this->subject = $data['subject'];
                $this->body = $data['message'];
                $this->setHeaders(isset($data['headers']) ? $data['headers'] : null);
                break;

        }


    }

    /**
     * @param null $headers
     */
    protected function setHeaders($headers = null) {
        if (!$headers) {
            $headers = '';

            $name = get_option('blogname');
            $email = get_option('admin_email');

            if (!empty($this->email_settings['from_name'])) {
                $name = $this->email_settings['from_name'];
            }

            if (!empty($this->email_settings['from_email'])) {
                $email = $this->email_settings['from_email'];
            }

            $headers .= "From: {$name} <{$email}>\r\n";
            $headers .= "Reply-To: {$email}\r\n";
        }

        $this->headers = $headers;
    }

    /**
     * @return bool
     */
    function sendEmail() {
        add_filter('wp_mail_content_type', array(EmailSettings::class, 'set_html_mail_content_type'));
        $success = wp_mail($this->to, $this->subject, $this->body, $this->headers);
        remove_filter('wp_mail_content_type', array(EmailSettings::class, 'set_html_mail_content_type'));

        return $success;
    }

    /**
     * @return array|string|void
     */
    private function get_admin_email_id_s() {
        $to = '';

        if (!empty($this->email_settings['admin_notice_emails'])) {
            $to = explode("\n", $this->email_settings['admin_notice_emails']);
            $to = array_map('trim', $to);
            $to = array_filter($to);
        }

        if (empty($to)) {
            $to = get_bloginfo('admin_email');
        }

        return $to;
    }

    /**
     */
    public function setEmailSettings() {

        $this->email_settings = Functions::get_option('rtcl_email_settings');

        $site_name = get_bloginfo('name');
        $site_url = esc_url(home_url());
        $date_format = get_option('date_format');
        $time_format = get_option('time_format');
        $current_time = current_time('timestamp');
        $this->placeholders = array(
            '{site_name}' => esc_html($site_name),
            '{site_link}' => sprintf('<a href="%s">%s</a>', $site_url, $site_name),
            '{site_url}'  => sprintf('<a href="%s">%s</a>', $site_url, $site_url),
            '{today}'     => date_i18n($date_format, $current_time),
            '{now}'       => date_i18n($date_format . ' ' . $time_format, $current_time)
        );
    }


    /**
     * Builds Email Headers
     *
     * @return string email headers
     */
    private function buildHeaders() {
        $headers = '';

        $headers .= implode("\r\n", $this->headers) . "\r\n";

        foreach ($this->bcc as $bcc) {
            $headers .= sprintf("Bcc: %s \r\n", $bcc);
        }

        foreach ($this->cc as $cc) {
            $headers .= sprintf("Cc: %s \r\n", $cc);
        }

        if (!empty($this->from)) {
            $headers .= sprintf("From: %s \r\n", $this->from);
        }

        return $headers;
    }


    /**
     * Set the wp_mail_content_type filter, if necessary
     */
    private function beforeSend() {
        if (count($this->to) === 0) {
            throw new Exception('You must set at least 1 recipient');
        }

        if ($this->sendAsHTML) {
            add_filter('wp_mail_content_type', array(EmailSettings::class, 'set_html_mail_content_type'));
        }
    }

    private function afterSend() {
        if ($this->sendAsHTML) {
            remove_filter('wp_mail_content_type', array(EmailSettings::class, 'set_html_mail_content_type'));
        }
    }


    /**
     * Sends a rendered email using
     * WordPress's wp_mail() function
     *
     * @return bool
     * @throws Exception
     */
    public function send() {
        $this->beforeSend();

        $mail = wp_mail($this->to, $this->subject, $this->render(), $this->buildHeaders(), $this->attachments);

        $this->afterSend();

        return $mail;
    }

}