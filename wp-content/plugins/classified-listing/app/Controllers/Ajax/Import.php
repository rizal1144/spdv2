<?php
namespace Rtcl\Controllers\Ajax;


class Import {

	function __construct(){
		add_action( 'wp_ajax_rtcl_import_location', array( $this, 'rtcl_import_location' ) );
		add_action( 'wp_ajax_rtcl_import_category', array( $this, 'rtcl_import_category' ) );
	}

	function rtcl_import_category(){
		$category = $_REQUEST['category'];
		if($category){
			$this->create_category($category);
		}
		wp_send_json(array('success'=> true));
	}
	function rtcl_import_location(){
		$location = $_REQUEST['location'];
		if($location){
			$this->create_location($location);
		}
		wp_send_json(array('success'=> true));
	}

	private function create_category($category, $parent = 0){

		$loc = wp_parse_args($category, array(
			"name" => '',
			"order" => 0,
			"child" => array()
		));
		if($loc['name']) {
			$loc_exist = term_exists( $loc['name'], rtcl()->category );
			if ( $loc_exist == 0 && $loc_exist == null ) {
				$term = wp_insert_term( $loc['name'], rtcl()->category, array('parent' => $parent) );
				if ( ! is_wp_error( $term ) ) {
					update_term_meta( $term['term_id'], "_rtcl_order", absint( $loc['order'] ) );
					if(!empty($loc['child'])){
						foreach ($loc['child'] as $subLoc){
							$this->create_category($subLoc, $term['term_id']);
						}
					}
				}
			}
		}
	}

	private function create_location($location, $parent = 0){

		$loc = wp_parse_args($location, array(
			"name" => '',
			"order" => 0,
			"child" => array()
		));
		if($loc['name']) {
			$loc_exist = term_exists( $loc['name'], rtcl()->location );
			if ( $loc_exist == 0 && $loc_exist == null ) {
				$term = wp_insert_term( $loc['name'], rtcl()->location, array('parent' => $parent) );
				if ( ! is_wp_error( $term ) ) {
					update_term_meta( $term['term_id'], "_rtcl_order", absint( $loc['order'] ) );
					if(!empty($loc['child'])){
						foreach ($loc['child'] as $subLoc){
							$this->create_location($subLoc, $term['term_id']);
						}
					}
				}
			}
		}
	}

}