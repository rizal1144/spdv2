<?php

namespace Rtcl\Controllers\Ajax;


use Rtcl\Helpers\Functions;
use Rtcl\Helpers\Link;
use Rtcl\Models\Payment;
use Rtcl\Models\RtclEmail;

/**
 * Class Checkout
 * @package Rtcl\Controllers\Ajax
 */
class Checkout {

    function __construct()
    {
        add_action('wp_ajax_rtcl_ajax_checkout_action', array($this, 'rtcl_ajax_checkout_action'));
    }

    function rtcl_ajax_checkout_action()
    {

        Functions::clear_notices();
        $success = false;
        $redirect_url = $gateway_id = null;
        if (isset($_POST['rtcl_checkout_nonce']) && wp_verify_nonce($_POST['rtcl_checkout_nonce'], 'rtcl_checkout')) {

            $post_id = (int) $_POST['post_id'];
            $payment_option_id = (int) $_POST['payment_option_id'];
            if ($post_id && $payment_option_id) {
                // place order
                $new_payment = array(
                    'post_title'  => sprintf(__('[Order] Listing #%d', 'classified-listing'), $post_id),
                    'post_status' => 'rtcl-created',
                    'post_author' => 1,
                    'post_type'   => rtcl()->post_type_payment
                );

                $payment_id = wp_insert_post($new_payment);

                if ($payment_id) {
                    $payment = new Payment($payment_id);
                    $payment->set_order_key();
                    // save meta fields
                    update_post_meta($payment_id, 'listing_id', $post_id);
                    update_post_meta($payment_id, 'customer_id', get_current_user_id());
                    update_post_meta($payment_id, 'customer_ip_address', Functions::get_ip_address());

                    do_action('rtcl_payment_created_hook', $payment_id);


                    $amount = 0.00;
                    $payment_option = get_post($payment_option_id);
                    if (is_object($payment_option) && $payment_option->post_type == rtcl()->post_type_pricing) {
                        $amount = get_post_meta($payment_option->ID, 'price', true);
                        $amount = Functions::get_formatted_amount($amount, true);
                        update_post_meta($payment_id, 'payment_option_id', $payment_option->ID);
                    }
                    update_post_meta($payment_id, 'amount', $amount);

                    $gateway = Functions::get_payment_gateway(sanitize_key($_POST['payment_method']));
                    if ($gateway) {
                        update_post_meta($payment_id, 'payment_method', $gateway->id);
                        $gateway_id = $gateway->id;
                    }

                    // process payment
                    if ($amount > 0) {

                        $result = $gateway->process_payment($payment_id);
                        $result = apply_filters('rtcl_payment_successful_result', $result, $payment_id);
                        $redirect_url = isset($result['redirect']) ? $result['redirect'] : null;
                        // Redirect to success/confirmation/payment page
                        if (isset($result['result']) && 'success' === $result['result']) {
                            $post = get_post($post_id);
                            if ($post && $post->post_type === rtcl()->post_type && $post->post_status === "publish") {
                                try {
                                    Functions::apply_payment_pricing($post->ID);
                                } catch (\Exception $e) {

                                }
                            }
                            $success = true;
                            if ($gateway_id == 'paypal') {
                                Functions::add_notice(__("Redirecting to paypal.", "classified-listing"), 'success');
                            }else if ($gateway_id == 'offline') {
                                Functions::add_notice(__("Payment made pending confirmation.", "classified-listing"), 'success');
                            } else {
                                Functions::add_notice(__("Payment successfully paid.", "classified-listing"), 'success');
                            }
                            if (Functions::get_option_item('rtcl_email_settings', 'notify_admin', 'order_created', 'multi_checkbox')) {
                                $email = new RtclEmail('order_created_admin');
                                $email->setData(array('payment_id' => $payment_id));
                                $email->sendEmail();
                            }

                            if (Functions::get_option_item('rtcl_email_settings', 'notify_users', 'order_created', 'multi_checkbox')) {
                                $email = new RtclEmail('order_created');
                                $email->setData(array('payment_id' => $payment_id));
                                $email->sendEmail();
                            }
                        } else {
                            wp_delete_post($payment_id);
                            if (!empty($result['message'])) {
                                Functions::add_notice($result['message'], 'error');
                            }
                        }

                    } else {
                        $payment = new Payment($payment_id);
                        $payment->payment_complete(wp_generate_password(12, true));
                        $redirect_url = Link::get_payment_receipt_page_link($payment_id);
                        Functions::add_notice(__("Payment successfully paid.", "classified-listing"), 'success');
                    }
                } else {
                    Functions::add_notice(__("Error to create order.", "classified-listing"), 'error');
                }
            } else {
                Functions::add_notice(__("Post Id and payment option id missing.", "classified-listing"), 'error');
            }

        } else {
            Functions::add_notice(__("Session error", "classified-listing"), 'error');
        }

        $error_message = Functions::get_notices('error');
        $success_message = Functions::get_notices('success');
        if (!$success) {
            Functions::clear_notices();
        }
        wp_send_json(array(
            'error_message'   => $error_message,
            'success_message' => $success_message,
            'success'         => $success,
            'redirect_url'    => $redirect_url,
            'gateway_id'      => $gateway_id
        ));

    }

}