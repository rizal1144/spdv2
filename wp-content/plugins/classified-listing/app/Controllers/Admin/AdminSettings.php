<?php

namespace Rtcl\Controllers\Admin;


use Rtcl\Helpers\Functions;
use Rtcl\Models\SettingsAPI;

class AdminSettings extends SettingsAPI
{

    private $tabs = array();
    private $active_tab;
    private $current_section;
    private $gateway_temp_desc;


    public function __construct() {
        add_action('admin_init', array($this, 'setTabs'));
        add_action('admin_init', array($this, 'save'));
        add_action('admin_menu', array($this, 'add_listing_types_menu'), 1);
        add_action('admin_menu', array($this, 'add_settings_menu'), 50);
        add_action('admin_menu', array($this, 'add_import_menu'), 60);
        add_action('rtcl_admin_settings_groups', array($this, 'setup_settings'));
        add_filter('plugin_action_links_classified-listing/classified-listing.php', array($this, 'rtcl_marketing'));
    }

    function rtcl_marketing($links) {
        $links[] = '<a target="_blank" href="' . esc_url('https://radiustheme.com/demo/wordpress/classified') . '">' . __('Demo', 'classified-listing') . '</a>';
        $links[] = '<a target="_blank" href="' . esc_url('https://www.radiustheme.com/setup-configure-classified-listing-wordpress/') . '">' . __('Documentation', 'classified-listing') . '</a>';
        $links[] = '<a target="_blank" style="color: #39b54a;font-weight: 700;" href="' . esc_url('https://www.radiustheme.com/downloads/classified-listing-pro-wordpress/') . '">' . __('Get Pro', 'classified-listing') . '</a>';
        return $links;
    }

    public function add_listing_types_menu() {
        add_submenu_page(
            'edit.php?post_type=' . rtcl()->post_type,
            __('Listing Types', 'classified-listing'),
            __('Listing Types', 'classified-listing'),
            'manage_rtcl_options',
            'rtcl-listing-type',
            array($this, 'display_listing_type')
        );
    }

    public function add_import_menu() {
        add_submenu_page(
            'edit.php?post_type=' . rtcl()->post_type,
            __('Import', 'classified-listing'),
            __('Import', 'classified-listing'),
            'manage_rtcl_options',
            'rtcl-import-export',
            array($this, 'display_import_export')
        );
    }

    public function add_settings_menu() {

        add_submenu_page(
            'edit.php?post_type=' . rtcl()->post_type,
            __('Settings', 'classified-listing'),
            __('Settings', 'classified-listing'),
            'manage_rtcl_options',
            'rtcl-settings',
            array($this, 'display_settings_form')
        );

    }

    function display_listing_type() {
        require_once RTCL_PATH . 'views/settings/listing-type.php';
    }

    function display_settings_form() {
        require_once RTCL_PATH . 'views/settings/admin-settings-display.php';
    }

    function display_import_export() {
        require_once RTCL_PATH . 'views/settings/import-export.php';
    }


    function setup_settings() {
        if ($this->current_section && $this->active_tab == 'payment') {
            $gateway = Functions::get_payment_gateway($this->current_section);
            if ($gateway) {
                $gateway->init_form_fields();
                $gateway->option = $this->option;
                $this->form_fields = $gateway->form_fields;
            }
        } else {
            $this->set_fields();
        }
        $this->admin_options();
    }

    function set_fields() {
        $field = array();
        $file_name = RTCL_PATH . "views/settings/{$this->active_tab}-settings.php";
        if (file_exists($file_name)) {
            $field = include($file_name);
        }

        $this->form_fields = apply_filters('rtcl_settings_option_fields', $field, $this->active_tab);
    }

    public function settings_section_callback($args) {

        switch ($args['id']) {
            case 'rtcl_date_time_settings_section':
                printf('<a href="%s">%s</a>',
                    "http://codex.wordpress.org/Formatting_Date_and_Time",
                    __('Documentation on date and time formatting', 'classified-listing'));
                break;

            case 'rtcl_payment_currency_settings_section' :
            case 'rtcl_currency_settings_section' :
                printf('<div id="currency-settings">%s</div>',
                    __('The following options affect how prices are displayed on the frontend.',
                        'classified-listing'));
                break;
            case 'rtcl_gateway_settings_section' :
                printf('%s <a href="%s">%s</a>',
                    __('Note: Currency settings under this section are used only to accept payments from your users.',
                        'classified-listing'),
                    admin_url('edit.php?post_type=listing&page=rtcl_settings&tab=general#currency-settings'),
                    __('Configure listing currency', 'classified-listing'));
                break;
            case 'rtcl_permalink_settings_section' :
                _e('NOTE: Just make sure that, after updating the fields in this section, you flush the rewrite rules by visiting Settings > Permalinks. Otherwise you\'ll still see the old links.',
                    'classified-listing');
                break;
            case 'rtcl_featured_listing_settings_section' :
                _e('Featured listings will always appear on top of regular listings.',
                    'classified-listing');
                break;
            case 'rtcl_gateway_offline_settings_section' :
                _e('Note: There\'s nothing automatic in this offline payment system, you should use this when you don\'t want to collect money automatically. So once money is in your bank account you change the status of the order manually under "Payment History" menu.',
                    'classified-listing');
                break;
            case 'rtcl_email_settings_renewal_reminder_section' :
                _e('Sent some time after listing expiration and when no renewal has occurred.',
                    'classified-listing');
                break;
            case 'rtcl_email_settings_listing_renewal_section' :
                _e('Sent at the time of listing expiration.', 'classified-listing');
                break;
            case 'rtcl_email_settings_renewal_pending_section' :
                _e('Sent some time before the listing expires.', 'classified-listing');
                break;
            case 'rtcl_email_settings_payment_abandoned_section' :
                _e('Sent some time after a pending payment is abandoned by users.',
                    'classified-listing');
                break;
            case 'rtcl_email_settings_listing_contact_section' :
                _e('Sent to listing owners when someone uses the contact form on their listing pages.',
                    'classified-listing');
                break;
            case 'rtcl_email_settings_listing_published_section' :
                _e('Sent when the listing has been published or approved by an admin.',
                    'classified-listing');
                break;
            case 'rtcl_email_settings_listing_received_section' :
                _e('Sent after a listing has been submitted.', 'classified-listing');
                break;
        }

    }

    private function payment_subsections() {
        $sections = array(
            '' => __("Checkout option", "classified-listing")
        );
        $payment_gateways = rtcl()->payment_gateways();
        foreach ($payment_gateways as $gateway) {
            $title = empty($gateway->method_title) ? ucfirst($gateway->id) : $gateway->method_title;
            $sections[strtolower($gateway->id)] = esc_html($title);
        }
        $this->subtabs = $sections;
    }

    public function payment_sub_section_section_callback() {
        echo "<p>" . wp_kses($this->gateway_temp_desc,
                array('a' => array('href' => array(), 'title' => array()))) . "</p>";
    }

    public function save() {
        if ('POST' !== $_SERVER['REQUEST_METHOD']
            || !isset($_REQUEST['post_type'])
            || !isset($_REQUEST['page'])
            || (isset($_REQUEST['post_type']) && rtcl()->post_type !== $_REQUEST['post_type'])
            || (isset($_REQUEST['rtcl_settings']) && 'rtcl_settings' !== $_REQUEST['rtcl_settings'])
        ) {
            return;
        }
        if (empty($_REQUEST['_wpnonce']) || !wp_verify_nonce($_REQUEST['_wpnonce'], 'rtcl-settings')) {
            die(__('Action failed. Please refresh the page and retry.', 'classified-listing'));
        }
        if ($this->current_section && $this->active_tab == 'payment') {
            $gateway = Functions::get_payment_gateway($this->current_section);
            if ($gateway) {
                $gateway->init_form_fields();
                $gateway->option = $this->option;
                $this->form_fields = $gateway->form_fields;
            }
        } else {
            $this->set_fields();
        }
        $this->process_admin_options();

        self::add_message(__('Your settings have been saved.', 'classified-listing'));

        do_action('rtcl_admin_settings_saved', $this->option);
    }

    function setTabs() {
        $this->tabs = array(
            'general'    => __('General', 'classified-listing'),
            'moderation' => __('Moderation', 'classified-listing'),
            'payment'    => __('Payment', 'classified-listing'),
            'email'      => __('Email', 'classified-listing'),
            'account'    => __('Account & Policy', 'classified-listing'),
            'style'      => __('Style', 'classified-listing'),
            'misc'       => __('Misc', 'classified-listing'),
            'advanced'   => __('Advanced', 'classified-listing'),
            'tools'      => __('Tools', 'classified-listing'),
        );

        // Hook to register custom tabs
        $this->tabs = apply_filters('rtcl_register_settings_tabs', $this->tabs);
        // Find the active tab
        $this->option = $this->active_tab = isset($_GET['tab']) && array_key_exists($_GET['tab'],
            $this->tabs) ? $_GET['tab'] : 'general';
        if ($this->active_tab == 'payment') {
            $this->payment_subsections();
        }
        if (!empty($this->subtabs)) {
            $this->current_section = isset($_GET['section']) && in_array($_GET['section'],
                array_filter(array_keys($this->subtabs))) ? $_GET['section'] : '';
            $this->option = !empty($this->current_section) ? $this->option . '_' . $this->current_section : $this->active_tab . "_settings";
        } else {
            $this->option = $this->option . "_settings";
        }

    }

}