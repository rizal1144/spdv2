<?php

namespace Rtcl\Controllers\Admin;


use Rtcl\Helpers\Functions;

class EmailSettings {

    public function __construct() {
//		add_action( 'phpmailer_init', array($this,'mailer_config'), 10, 1);
        add_action( 'wp_mail_failed', array( $this, 'log_mailer_errors' ) );
    }

    function mailer_config( \PHPMailer $phpMailer ) {

        $settings = Functions::get_option( 'rtcl_email_settings' );

        // smtp not enabled?
        $enabled = ! empty( $settings['usesmtp'] ) ? $settings['usesmtp'] : false;
        if ( ! $enabled || 0 == $enabled ) {
            return;
        }
        $hostname = ! empty( $settings['smtphost'] ) ? $settings['smtphost'] : null;
        $port     = ! empty( $settings['smtpport'] ) ? $settings['smtpport'] : null;
        $username = ! empty( $settings['smtpusername'] ) ? $settings['smtpusername'] : null;
        $password = ! empty( $settings['smtppassword'] ) ? $settings['smtppassword'] : null;
        // host and port not set? gotta have both.
        if ( '' == trim( $hostname ) || '' == trim( $port ) ) {
            return;
        }
        // still got defaults set? can't use those.
        if ( 'mail.example.com' == trim( $hostname ) ) {
            return;
        }
        if ( 'smtp_username' == trim( $username ) ) {
            return;
        }
        $phpMailer->Mailer = 'smtp';
        $phpMailer->Host   = $hostname;
        $phpMailer->Port   = $port;
        // If there's a username and password then assume SMTP Auth is necessary and set the vars:
        if ( '' != trim( $username ) && '' != trim( $password ) ) {
            $phpMailer->SMTPAuth = true;
            $phpMailer->Username = $username;
            $phpMailer->Password = $password;
        }
        // that's it!
    }

    function log_mailer_errors( $mailer ) {
        $fn = ABSPATH . '/wp-content/mail.log'; // say you've got a mail.log file in your server root
	    if($mailer) {
		    $fp = fopen( $fn, 'a' );
		    fputs( $fp, "Mailer Error: " . $mailer->ErrorInfo . "\n" );
		    fclose( $fp );
	    }
    }

    /**
     * @param $content_type
     *
     * @return string
     */
    static function set_html_mail_content_type( $content_type ) {
        return 'text/html';
    }

}