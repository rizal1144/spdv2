<?php

namespace Rtcl\Controllers\Admin;

class Roles {

    public function add_caps()
    {
        global $wp_roles;

        if (class_exists('\WP_Roles')) {
            if (!isset($wp_roles)) {
                $wp_roles = new \WP_Roles();
            }
        }

        if (is_object($wp_roles)) {

            // Add the "administrator" capabilities
            $capabilities = $this->get_core_caps();
            foreach ($capabilities as $cap_group) {
                foreach ($cap_group as $cap) {
                    $wp_roles->add_cap('administrator', $cap);
                }
            }

            // Add the "editor" capabilities
            $wp_roles->add_cap('editor', 'add_' . rtcl()->post_type);
            $wp_roles->add_cap('editor', 'edit_' . rtcl()->post_type . 's');
            $wp_roles->add_cap('editor', 'edit_others_' . rtcl()->post_type . 's');
            $wp_roles->add_cap('editor', 'publish_' . rtcl()->post_type . 's');
            $wp_roles->add_cap('editor', 'read_private_' . rtcl()->post_type . 's');
            $wp_roles->add_cap('editor', 'delete_' . rtcl()->post_type . 's');
            $wp_roles->add_cap('editor', 'delete_private_' . rtcl()->post_type . 's');
            $wp_roles->add_cap('editor', 'delete_published_' . rtcl()->post_type . 's');
            $wp_roles->add_cap('editor', 'delete_others_' . rtcl()->post_type . 's');
            $wp_roles->add_cap('editor', 'edit_private_' . rtcl()->post_type . 's');
            $wp_roles->add_cap('editor', 'edit_published_' . rtcl()->post_type . 's');

            // Add the "author" capabilities
            $wp_roles->add_cap('author', 'add_' . rtcl()->post_type);
            $wp_roles->add_cap('author', 'edit_' . rtcl()->post_type . 's');
            $wp_roles->add_cap('author', 'publish_' . rtcl()->post_type . 's');
            $wp_roles->add_cap('author', 'delete_' . rtcl()->post_type . 's');
            $wp_roles->add_cap('author', 'delete_published_' . rtcl()->post_type . 's');
            $wp_roles->add_cap('author', 'edit_published_' . rtcl()->post_type . 's');

            // Add the "contributor" capabilities
            $wp_roles->add_cap('contributor', 'add_' . rtcl()->post_type);
            $wp_roles->add_cap('contributor', 'edit_' . rtcl()->post_type . 's');
            $wp_roles->add_cap('contributor', 'publish_' . rtcl()->post_type . 's');
            $wp_roles->add_cap('contributor', 'delete_' . rtcl()->post_type . 's');
            $wp_roles->add_cap('contributor', 'delete_published_' . rtcl()->post_type . 's');
            $wp_roles->add_cap('contributor', 'edit_published_' . rtcl()->post_type . 's');

            // Add the "subscriber" capabilities
            $wp_roles->add_cap('subscriber', 'add_' . rtcl()->post_type);
            $wp_roles->add_cap('subscriber', 'edit_' . rtcl()->post_type . 's');
            $wp_roles->add_cap('subscriber', 'publish_' . rtcl()->post_type . 's');
            $wp_roles->add_cap('subscriber', 'delete_' . rtcl()->post_type . 's');
            $wp_roles->add_cap('subscriber', 'delete_published_' . rtcl()->post_type . 's');
            $wp_roles->add_cap('subscriber', 'edit_published_' . rtcl()->post_type . 's');

        }
    }

    public function get_core_caps()
    {

        $capabilities = array();
        $capabilities['core'] = array('manage_rtcl_options');
        $capability_types = array(rtcl()->post_type);

        foreach ($capability_types as $capability_type) {

            $capabilities[$capability_type] = array(
                "add_{$capability_type}",
                "edit_{$capability_type}",
                "read_{$capability_type}",
                "delete_{$capability_type}",
                "edit_{$capability_type}s",
                "edit_others_{$capability_type}s",
                "publish_{$capability_type}s",
                "read_private_{$capability_type}s",
                "delete_{$capability_type}s",
                "delete_private_{$capability_type}s",
                "delete_published_{$capability_type}s",
                "delete_others_{$capability_type}s",
                "edit_private_{$capability_type}s",
                "edit_published_{$capability_type}s",
            );
        }

        return $capabilities;

    }

    public function meta_caps($caps, $cap, $user_id, $args)
    {

        // If editing, deleting, or reading a listing, get the post and post type object.
        if ('edit_' . rtcl()->post_type == $cap || 'delete_' . rtcl()->post_type == $cap || 'read_' . rtcl()->post_type == $cap) {
            $post = get_post($args[0]);
            $post_type = get_post_type_object($post->post_type);

            // Set an empty array for the caps.
            $caps = array();
        }

        // If editing a listing, assign the required capability.
        if ('edit_' . rtcl()->post_type == $cap) {
            if ($user_id == $post->post_author) {
                $caps[] = $post_type->cap->edit_listings;
            } else {
                $caps[] = $post_type->cap->edit_others_listings;
            }
        } // If deleting a listing, assign the required capability.
        else if ('delete_' . rtcl()->post_type == $cap) {
            if ($user_id == $post->post_author) {
                $caps[] = $post_type->cap->delete_listings;
            } else {
                $caps[] = $post_type->cap->delete_others_listings;
            }
        } // If reading a private listing, assign the required capability.
        else if ('read_' . rtcl()->post_type == $cap) {
            if ('private' != $post->post_status) {
                $caps[] = 'read';
            } elseif ($user_id == $post->post_author) {
                $caps[] = 'read';
            } else {
                $caps[] = $post_type->cap->read_private_listings;
            }
        }

        // Return the capabilities required by the user.
        return $caps;

    }

    public function remove_caps()
    {

        global $wp_roles;

        if (class_exists('WP_Roles')) {
            if (!isset($wp_roles)) {
                $wp_roles = new \WP_Roles();
            }
        }

        if (is_object($wp_roles)) {

            // Remove the "administrator" Capabilities
            $capabilities = $this->get_core_caps();

            foreach ($capabilities as $cap_group) {
                foreach ($cap_group as $cap) {
                    $wp_roles->remove_cap('administrator', $cap);
                }
            }
            // Remove the "editor" capabilities
            $wp_roles->remove_cap('editor', 'edit_' . rtcl()->post_type . 's');
            $wp_roles->remove_cap('editor', 'edit_others_' . rtcl()->post_type . 's');
            $wp_roles->remove_cap('editor', 'publish_' . rtcl()->post_type . 's');
            $wp_roles->remove_cap('editor', 'read_private_' . rtcl()->post_type . 's');
            $wp_roles->remove_cap('editor', 'delete_' . rtcl()->post_type . 's');
            $wp_roles->remove_cap('editor', 'delete_private_' . rtcl()->post_type . 's');
            $wp_roles->remove_cap('editor', 'delete_published_' . rtcl()->post_type . 's');
            $wp_roles->remove_cap('editor', 'delete_others_' . rtcl()->post_type . 's');
            $wp_roles->remove_cap('editor', 'edit_private_' . rtcl()->post_type . 's');
            $wp_roles->remove_cap('editor', 'edit_published_' . rtcl()->post_type . 's');

            // Remove the "author" capabilities
            $wp_roles->remove_cap('author', 'edit_' . rtcl()->post_type . 's');
            $wp_roles->remove_cap('author', 'publish_' . rtcl()->post_type . 's');
            $wp_roles->remove_cap('author', 'delete_' . rtcl()->post_type . 's');
            $wp_roles->remove_cap('author', 'delete_published_' . rtcl()->post_type . 's');
            $wp_roles->remove_cap('author', 'edit_published_' . rtcl()->post_type . 's');

            // Remove the "contributor" capabilities
            $wp_roles->remove_cap('contributor', 'edit_' . rtcl()->post_type . 's');
            $wp_roles->remove_cap('contributor', 'publish_' . rtcl()->post_type . 's');
            $wp_roles->remove_cap('contributor', 'delete_' . rtcl()->post_type . 's');
            $wp_roles->remove_cap('contributor', 'delete_published_' . rtcl()->post_type . 's');
            $wp_roles->remove_cap('contributor', 'edit_published_' . rtcl()->post_type . 's');

            // Remove the "subscriber" capabilities
            $wp_roles->remove_cap('subscriber', 'edit_' . rtcl()->post_type . 's');
            $wp_roles->remove_cap('subscriber', 'publish_' . rtcl()->post_type . 's');
            $wp_roles->remove_cap('subscriber', 'delete_' . rtcl()->post_type . 's');
            $wp_roles->remove_cap('subscriber', 'delete_published_' . rtcl()->post_type . 's');
            $wp_roles->remove_cap('subscriber', 'edit_published_' . rtcl()->post_type . 's');

        }
    }
}
