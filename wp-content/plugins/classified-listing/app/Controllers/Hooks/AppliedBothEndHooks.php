<?php

namespace Rtcl\Controllers\Hooks;

use Rtcl\Models\RtclEmail;
use Rtcl\Helpers\Functions;
use Rtcl\Emails\EmailNewUserNotificationAdmin;
use Rtcl\Emails\EmailUpdatePostNotificationAdmin;

class AppliedBothEndHooks
{

    static public function init() {
        add_action('rtcl_new_user_created', array(__CLASS__, 'new_user_notification_email_admin'));
        add_action('rtcl_listing_form_after_save_or_update', array(__CLASS__, 'new_post_notification_email_user'), 10, 2);
        add_action('rtcl_listing_form_after_save_or_update', array(__CLASS__, 'new_post_notification_email_admin'), 20, 2);
        add_action('rtcl_listing_form_after_save_or_update', array(__CLASS__, 'update_post_notification_email_admin'), 30, 2);

        add_filter('rtcl_my_account_endpoint', array(__CLASS__, 'my_account_end_point_filter'), 10);
    }

    public static function my_account_end_point_filter($endpoints) {
        // Remove payment endpoint
        if (Functions::is_payment_disabled()) {
            unset($endpoints['payments']);
        }

        // Remove favourites endpoint
        if (Functions::is_favourites_disabled()) {
            unset($endpoints['favourites']);
        }

        return $endpoints;
    }

    static public function new_user_notification_email_admin($user_id) {
        if (Functions::get_option_item('rtcl_email_settings', 'notify_admin', 'register_new_user', 'multi_checkbox')) {
            new EmailNewUserNotificationAdmin($user_id);
        }
    }

    static public function update_post_notification_email_admin($post_id, $type) {
        if ($type == 'update' && Functions::get_option_item('rtcl_email_settings', 'notify_admin', 'listing_edited', 'multi_checkbox')) {
            new EmailUpdatePostNotificationAdmin($post_id);
        }
    }

    static public function new_post_notification_email_admin($post_id, $type) {
        if ($type == 'new' && Functions::get_option_item('rtcl_email_settings', 'notify_admin', 'listing_submitted', 'multi_checkbox')) {
            $email = new RtclEmail('listing_submitted_admin');
            $email->setData(array('post_id' => $post_id));
            $email->sendEmail();
        }
    }

    static public function new_post_notification_email_user($post_id, $type) {
        if ($type == 'new' && Functions::get_option_item('rtcl_email_settings', 'notify_users', 'listing_submitted', 'multi_checkbox')) {
            $email = new RtclEmail('listing_submitted');
            $email->setData(array('post_id' => $post_id));
            $email->sendEmail();
        }
    }

}