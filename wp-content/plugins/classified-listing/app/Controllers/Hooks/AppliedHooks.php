<?php

namespace Rtcl\Controllers\Hooks;


class AppliedHooks {

	public static function init() {
		add_filter('rtcl_get_the_price', array(__CLASS__, 'rtcl_get_the_price'), 10, 2);
	}


	public static function rtcl_get_the_price($html_price, $listing_id){
		$ad_type = get_post_meta($listing_id, 'ad_type', true);
		if($ad_type == 'job'){
			$html_price = '';
		}elseif($ad_type == 'to_let' && is_singular(rtcl()->post_type) && $position = strrpos($html_price, '</span>', -1)){
				$html_price = substr_replace( $html_price, sprintf("<span class='rtcl-per-unit'> / %s</span>",__("Month", "classified-listing")), $position, 0 );
		}
		return $html_price;
	}
}