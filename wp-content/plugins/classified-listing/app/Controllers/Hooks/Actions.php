<?php

namespace Rtcl\Controllers\Hooks;

class Actions {

    public static function init()
    {
        Filters::init();
        Hooks::init();
        AppliedBothEndHooks::init();
    }

}