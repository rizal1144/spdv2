<?php

namespace Rtcl\Emails;

use Rtcl\Helpers\Functions;
use Rtcl\Models\RtclEmail;

class EmailUpdatePostNotificationAdmin extends RtclEmail
{
    function __construct($post_id) {
        $post = get_post($post_id);
        if (!$post_id || !is_object($post)) {
            return;
        }
        $this->to(Functions::get_admin_email_id_s());
        $blogname = Functions::get_blogname();
        $subject = sprintf(__('[%s] Updated (%s)', "classified-listing"), $blogname, get_the_title($post));
        $subject = apply_filters('rtcl_update_post_notification_email_admin_subject', $subject, $post, $blogname);
        $this->subject($subject);

        $this->template('emails/update-post-notification-admin', compact('post', 'blogname'));

        $this->send();

    }

}