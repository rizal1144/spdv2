<?php

namespace Rtcl\Emails;

use Rtcl\Helpers\Functions;
use Rtcl\Models\RtclEmail;

class EmailUserResetPasswordNotificationUser extends RtclEmail {

	function __construct( $user, $reset_key ) {
		if ( ! $user || ! $reset_key ) {
			return;
		}
		$this->to( $user->user_email );
		$blogname = Functions::get_blogname();
		$subject  = sprintf( __( '[%s] Reset your password', 'classified-listing' ), $blogname );
		$subject  = apply_filters( 'rtcl_user_reset_password_notification_email_user_subject', $subject, $user, $blogname );

		$this->subject( $subject );

		$this->template( 'emails/user-reset-password-notification-user', compact( 'user', 'reset_key', 'blogname' ) );

		$this->send();

	}


}