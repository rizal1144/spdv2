<?php

namespace Rtcl\Emails;

use Rtcl\Helpers\Functions;
use Rtcl\Models\RtclEmail;

class EmailNewUserNotificationAdmin extends RtclEmail {
	function __construct( $user_id ) {
		if ( ! $user_id ) {
			return;
		}
		$user = get_userdata( $user_id );


		$this->to( Functions::get_admin_email_id_s() );
		$blogname = Functions::get_blogname();
		$subject  = sprintf( __( '[%s] New User is %s registered', "classified-listing" ), $blogname, $user->user_login );
		$subject  = apply_filters( 'rtcl_new_user_notification_email_admin_subject', $subject, $user, $blogname );
		$this->subject( $subject );

		$this->template( 'emails/new-user-notification-admin', compact( 'user', 'blogname' ) );

		$this->send();

	}

}