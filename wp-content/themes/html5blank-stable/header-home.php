<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_bloginfo('template_directory'); ?>/css/style.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300" rel="stylesheet">
   
     <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/main.css">
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/footer.css">
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/styles.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="<?php echo get_bloginfo('template_directory'); ?>/js/main.js"></script>

    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


</head>
<body>
   <section class="hero" id="intro" style="margin-bottom: -50px;">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-right navicon">
              <a id="nav-toggle" class="nav_slide_button" href="#"><span></span></a>
          </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-md-offset-2 text-center inner">
           <div class="animatedParent">
              <img src="<?php echo get_bloginfo('template_directory'); ?>/images/logo-sc.png">
          </div>
      </div>
  </div>    
</div>
</section>

<div class="row">
    <div class="col-md-12 col-md-offset-3 text-center">
      <a href="#inspired" class="fa fa-chevron-down" style="font-size: 50px; color: white;"></a>
  </div>
</div>

<!-- start navbar --> 
<header>
 <div id="navbar" style="padding-left: 0px" class="container-fluid">
    <div class="container nav-fill w-100">
       <nav class="navbar navbar-expand-md navbar-light" role="navigation">
          <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
             <span class="navbar-toggler-icon" style="background-color: white"></span>
         </button>
         <div class="collapse navbar-collapse" id="navbarContent">
             <ul class="navbar-nav nav-fill w-100">
                <li class="nav-item">
                   <a href="<?php echo site_url(); ?>/article" style="text-decoration: none; font-size: 12px; color: white;">ARTICLES</a>
               </li>
               <li class="nav-item">
                   <div class="btn-group">
                     <a href="#" style="text-decoration: none; font-size: 12px; color: white;">COMMUNITY</a>
                 </div>
             </li>
             <li class="nav-item">
               <div class="btn-group">
                 <a href="#" style="text-decoration: none; font-size: 12px; color: white;">WORKSHOP</a>
             </div>
         </li>
         <li class="nav-item">
           <div class="btn-group">
             <a href="<?php echo site_url(); ?>/marketplace4" style="text-decoration: none; font-size: 12px; color: white;">MARKETPLACE</a>
         </div>
     </li>
     <li class="nav-item">
       <div class="btn-group">
         <a href="#" data-toggle="modal" data-target="#myModal" style="text-decoration: none; font-size: 12px; color: white;">REGISTER | LOGIN</a>
     </div>
 </li>
</ul>
</div>
</nav>
</div>
</div>
</header>

<!-- end navbar -->

<!-- start pop up -->

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content" style="background-color: #ffffff00">

       

      <div class="container login-container" style="background-image: url(<?php echo get_bloginfo('template_directory'); ?>/images/login.png);">
        <div class="row">
            <div class="col-md-6 login-form-1">
                <h3 id="datalogin">LOG IN</h3>
                <h4 style="color: grey;text-align: center; margin-bottom: 12%;">to your account</h4>
                <div class="form-group">
                </div>
                <div class="form-group">
                    <center><input type="text" id="userpass" class="form-control" placeholder="Password" /></center>
                </div>
                <div class="form-group">
                    <center><input type="submit" class="btnSubmit1" value="LOG IN" /></center>
                </div>

                <h4 style="color: #5e5d5d;text-align: center;">Forget Password?</h4>

            </div>
            <div class="col-md-6 login-form-2" style="background-image: url(<?php echo get_bloginfo('template_directory'); ?>/images/register.png);"    >
                <div class="login-logo">
                    <img src="<?php echo get_bloginfo('template_directory'); ?>/images/or.jpg" alt=""/>
                </div>
                <h4 style="color: grey;text-align: center;">Not a member yet?</h4>
                <h3>REGISTER</h3>
                <div class="form-group">
                    <center><input type="text" id="regis" class="form-control" placeholder="Username" /></center>
                </div>
                <div class="form-group">
                    <center><input type="text"  id="regis" class="form-control" placeholder="Email Address" /></center>
                </div>
                <div class="form-group">
                    <center><input type="password"  id="regis" class="form-control" placeholder="Password" /></center>
                </div>
                <div class="form-group">
                    <center><input type="submit" class="btnSubmit" value="LOG IN" style="color: black;" /></center>
                </div>


            </div>
        </div>
    </div>

</div>
</div>
</div>
</div>
<!-- end pop up -->


<script type="text/javascript">
    $(function(){
        $(document).on('click',".modal-button", function(){
            var target = $(this).data('target');
            $("#" + target).addClass("is-active");
        })


        $(document).on('click',".modal-close", function(){
            $(this).closest($(".modal")).removeClass("is-active");
        })
    })
</script>