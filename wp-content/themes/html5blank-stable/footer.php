<?php

if(is_user_logged_in()){
  // tambahin div penutup class dashboard-body
  echo "</div>";
} 
?>

<!-- start footer -->

<div class="row" style=" background-color: #242424">

    <!-- Grid column -->
    <div class="col-md-12 mb-4">

        <!--Footer-->
        <footer class="page-footer center-on-small-only stylish-color-dark" style="background-image: url('<?php echo get_bloginfo('template_directory'); ?>/images/footer_bg.png');">
           <img class="logo-footer-sc" style="margin-left: 30px" src="<?php echo get_bloginfo('template_directory'); ?>/images/logo-sc.png">  
           <!--Footer Links-->
           <div class="container">
            <div class="row">

                <!--First column-->
                <div class="col-md-3">
                    <h5 class="title mb-4 mt-3 font-bold" style="text-align: left;">Articles</h5>

                    <p style="text-align: left; margin-left: 15%;"><a style="color:white" href="#">News</a></p>
                    <p style="text-align: left; margin-left: 15%;"><a style="color:white" href="#">Video</a></p>


                </div>
                <!--/.First column-->

                <hr class="clearfix w-100 d-md-none">

                <!--Second column-->
                <div class="col-md-3 mx-auto">
                    <h5 class="title mb-4 mt-3 font-bold" style="text-align: left;">COMMUNITY</h5>
                    <p style="text-align: left; margin-left: 15%;"><a style="color:white" href="#">Cars</a></p>
                    <p style="text-align: left; margin-left: 15%;"><a style="color:white" href="#">Clubs</a></p>
                    <p style="text-align: left; margin-left: 15%;"><a style="color:white" href="#">Members</a></p>
                    <p style="text-align: left; margin-left: 15%;"><a style="color:white" href="#">Membership Tiers</a></p>

                </div>
                <!--/.Second column-->

                <hr class="clearfix w-100 d-md-none">

                <!--Third column-->
                <div class="col-md-3 mx-auto">
                    <h5 class="title mb-4 mt-3 font-bold" style="text-align: left;">WORKSHOP</h5>
                    <p style="text-align: left; margin-left: 15%;"><a style="color:white" href="#">Map Locator</a></p>
                </div>
                <!--/.Third column-->

                <hr class="clearfix w-100 d-md-none">

                <!--Fourth column-->
                <div class="col-md-3 mx-auto">
                    <h5 class="title mb-4 mt-3 font-bold" style="text-align: left;">MARKETPLACE</h5>

                    <p style="text-align: left; margin-left: 15%;"><a style="color:white" href="#">Cars</a></p>
                    <p style="text-align: left; margin-left: 15%;"><a style="color:white" href="#">Parts</a></p>
                    <p style="text-align: left; margin-left: 15%;"><a style="color:white" href="#">Others</a></p>
                    <p style="text-align: left; margin-left: 15%;"><a style="color:white" href="#">E- Commerce</a></p>

                </div>
                <!--/.Fourth column-->
            </div>
        </div>
        <!--/.Footer Links-->

        <hr>

        <div class="row">
           <div class="col-md-4">

           </div>
           <div class="col-md-2">
              <p style="font-size: 20px; color: white;">SUBSCRIBE</p>
          </div>
          <div class="col-md-3">
              <input type="text" style="width: 80%; height: 30px">
          </div>
          <div class="col-md-3">

          </div>
      </div>

      <div class="row">
       <div class="col-md-6">

       </div>
       <div class="col-md-2">
          <i style="color: white; font-size: 20px" class="fa fa-facebook"></i> &nbsp;&nbsp;&nbsp;&nbsp;
          <i style="color: white; font-size: 20px" class="fa fa-instagram"></i> &nbsp;&nbsp;&nbsp;&nbsp;
          <i style="color: white; font-size: 20px" class="fa fa-google-plus"></i>
      </div>
      <div class="col-md-4">

      </div>

  </div>



</footer>
<!--/.Footer-->

</div>
<!-- Grid column -->

</div>


<!-- end footer -->

    </body>
</html>