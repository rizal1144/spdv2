<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Marketplace</title>
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/main.css">
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/footer.css">
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/styles.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    

</head>


<nav class="navbar fixed-top navbar-expand-md custom-navbar navbar-dark">
   <img class="navbar-brand" src="<?php echo get_bloginfo('template_directory'); ?>/images/logo-sc.png" id="logo_custom" width="15%"  alt="logo">
   <button class="navbar-toggler navbar-toggler-right custom-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon "></span>
</button>
<div class="collapse navbar-collapse " id="collapsibleNavbar">
  <ul class="navbar-nav ml-auto ">
    <li class="nav-item">
      <a class="nav-link" href="#">ARTICLES</a>
  </li>
  <li class="nav-item">
      <a class="nav-link" href="#">COMMUNITY</a>
  </li>
  <li class="nav-item">
      <a class="nav-link" href="#">WORKSHOPS</a>
  </li> 
  <li class="nav-item">
      <a class="nav-link" href="#">MARKETPLACE</a>
  </li>
  <li class="nav-item">
      <i class="fa fa-user-o" style="color: white; margin-top:8px; margin-right: 10px; font-size: 20px;"></i>
      <i class="fa fa-bell-o" style="color: white; margin-top:8px; margin-right: 10px; font-size: 20px;"></i>
      <i class="fa fa-gear" style="color: white; margin-top:8px; margin-right: 10px; font-size: 20px;"></i>

  </li>


</ul>
</div>  
</nav>


<style type="text/css">
img.wp-smiley,
img.emoji {
  display: inline !important;
  border: none !important;
  box-shadow: none !important;
  height: 1em !important;
  width: 1em !important;
  margin: 0 .07em !important;
  vertical-align: -0.1em !important;
  background: none !important;
  padding: 0 !important;
}
</style>


<link rel='stylesheet' id='wp-block-library-css'  href='http://localhost/buddy-test2/wp-includes/css/dist/block-library/style.min.css?ver=5.1.1' media='all' />
<link rel='stylesheet' id='bp-legacy-css-css'  href='http://localhost/buddy-test2/wp-content/plugins/buddypress/bp-templates/bp-legacy/css/buddypress.min.css?ver=4.2.0' media='screen' />
<style id='bp-legacy-css-inline-css' type='text/css'>

    /* Cover image */
    #buddypress #header-cover-image {
      margin-top: 50px;
      height: 255px;
      background-image: url(http://localhost/buddy-test/wp-content/uploads/buddypress/members/1/cover-image/5d0748254f2bd-bp-cover-image.jpg);
    }

    #buddypress #create-group-form #header-cover-image {
      margin: 1em 0;
      position: relative;
    }

    .bp-user #buddypress #item-header {
      padding-top: 0;
    }

    #buddypress #item-header-cover-image #item-header-avatar {
      margin-top: 145px;
      float: left;
      overflow: visible;
      width: auto;
    }

    #buddypress div#item-header #item-header-cover-image #item-header-content {
      clear: both;
      float: left;
      margin-left: 170px;
      margin-top: -140px;
      width: auto;
    }

    body.single-item.groups #buddypress div#item-header #item-header-cover-image #item-header-content,
    body.single-item.groups #buddypress div#item-header #item-header-cover-image #item-actions {
      clear: none;
      margin-top: 225px;
      margin-left: 0;
      max-width: 50%;
    }

    body.single-item.groups #buddypress div#item-header #item-header-cover-image #item-actions {
      max-width: 20%;
      padding-top: 20px;
    }

    

    #buddypress div#item-header-cover-image .user-nicename a,
    #buddypress div#item-header-cover-image .user-nicename {
      font-size: 200%;
      color: #fff;
      margin: 0 0 0.6em;
      text-rendering: optimizelegibility;
      text-shadow: 0 0 3px rgba( 0, 0, 0, 0.8 );
    }

    #buddypress #item-header-cover-image #item-header-avatar img.avatar {
      background: rgba( 255, 255, 255, 0.8 );
      border: solid 2px #fff;
      border-radius:100px;
    }

    #buddypress #item-header-cover-image #item-header-avatar a {
      border: 0;
      text-decoration: none;
    }

    #buddypress #item-header-cover-image #item-buttons {
      margin: 0 0 10px;
      padding: 0 0 5px;
    }

    #buddypress #item-header-cover-image #item-buttons:after {
      clear: both;
      content: "";
      display: table;
    }

    @media screen and (max-width: 782px) {
      #buddypress #item-header-cover-image #item-header-avatar,
      .bp-user #buddypress #item-header #item-header-cover-image #item-header-avatar,
      #buddypress div#item-header #item-header-cover-image #item-header-content {
        width: 100%;
        text-align: center;
      }

      #buddypress #item-header-cover-image #item-header-avatar a {
        display: inline-block;
      }

      #buddypress #item-header-cover-image #item-header-avatar img {
        margin: 0;
      }

      #buddypress div#item-header #item-header-cover-image #item-header-content,
      body.single-item.groups #buddypress div#item-header #item-header-cover-image #item-header-content,
      body.single-item.groups #buddypress div#item-header #item-header-cover-image #item-actions {
        margin: 0;
      }

      body.single-item.groups #buddypress div#item-header #item-header-cover-image #item-header-content,
      body.single-item.groups #buddypress div#item-header #item-header-cover-image #item-actions {
        max-width: 100%;
      }

      #buddypress div#item-header-cover-image h2 a,
      #buddypress div#item-header-cover-image h2 {
        color: inherit;
        text-shadow: none;
        margin: 25px 0 0;
        font-size: 200%;
      }

      #buddypress #item-header-cover-image #item-buttons div {
        float: none;
        display: inline-block;
      }

      #buddypress #item-header-cover-image #item-buttons:before {
        content: "";
      }

      #buddypress #item-header-cover-image #item-buttons {
        margin: 5px 0;
      }
    }
  
</style>


