<?php get_header('menu'); ?>

<body>
    <section>
        <div id="section1-option" class="container-fluid">
            <div class="row">

                <div id="section1-option1" class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
                    <a href="#">
                        <h4 class="section1-title">CAR &amp; PARTS</h4>
                    </a>
                </div>


                <div id="section1-option2" class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
                    <a href="#">
                         <h4 class="section1-title">OTHERS</h4>
                    </a>
                </div>


                <div id="section1-option3" class="col-md-4 col-lg-4 col-xs-4 col-sm-4">
                    <a href="#">
                        <h4 class="section1-title">SHOP</h4>
                    </a>
                </div>



            </div>
        </div>
    </section>

    <section class="section2" class="mobile">
        <!-- sidebar search -->
        <div id="section2-container" class="container">
            <div class="row">
                <div class="col-md-4">
                    <h5 class="sortby">SORT BY:</h5>
                    <br>
                    <h5 class="sortby">CAR / PARTS</h5>
                    <br>
                    <div class="dropdown">
                        <button id="dropdown-styling" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" placeholder="Click here">
                              Select Categories
                            </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">CAR</a>
                            <a class="dropdown-item" href="#">PART</a>
                        </div>
                    </div>
                    <br>
                    <hr style="border: 1px solid black">
                    <br>
                    <div class="dropdown">
                        <button id="dropdown-styling" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" placeholder="Click here">
                                  CAR MAKE
                                </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">CAR</a>
                            <a class="dropdown-item" href="#">PART</a>
                        </div>
                    </div>
                    <br>
                    <div class="dropdown">
                        <button id="dropdown-styling" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" placeholder="Click here">
                                      MODEL
                                    </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">CAR</a>
                            <a class="dropdown-item" href="#">PART</a>
                        </div>
                    </div>
                    <br>
                    <div class="dropdown">
                        <button id="dropdown-styling" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" placeholder="Click here">
                                  YEAR
                                </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">CAR</a>
                            <a class="dropdown-item" href="#">PART</a>
                        </div>
                    </div>
                    <br>
                    <hr style="border: 1px solid black">
                    <br>
                    <h5 class="sortby">PRICE RANGE</h5>
                    <br>
                    <div class="dropdown">
                        <button id="dropdown-styling" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" placeholder="Click here">
                              0 &nbsp;&nbsp; IDR
                            </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">1,000,000,000</a>

                        </div>
                        <br>
                        <br>
                        <div class="dropdown">
                            <button id="dropdown-styling" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" placeholder="Click here">
                                      70,000,000 &nbsp;&nbsp; IDR
                                    </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">1,000,000,000</a>

                            </div>
                            <br>
                            <br>
                            <a href="#">
                            <button  id="search-btn" class="btn-lg">Search</button>    
                            </a>


                        </div>
                        <!-- sidebar search -->

                        
                    </div>

                </div>
                <div class="col-md-8">
                        <div id="search-result-featured" class="card">
                                <img class="card-img-top" src="<?php echo get_bloginfo('template_directory'); ?>/images/kartik-bhattacharjee-257547-unsplash.png" alt="Card image cap">
                                <div class="card-body">
                                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                </div>
                        </div>
                        
                        <div class="row">
                            
                                
                                <div  class="col-md-4">
                                        <div  id="search-results" class="card" style="width: 15rem;">
                                            <img class="card-img-top" src="<?php echo get_bloginfo('template_directory'); ?>/images/marcus-p-655824-unsplash.png" alt="Card image cap">
                                            <div class="card-body">
                                              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                            </div>
                                        </div>
                                </div>
                                <div  class="col-md-4">
                                        <div  id="search-results" class="card" style="width: 15rem;">
                                            <img class="card-img-top" src="<?php echo get_bloginfo('template_directory'); ?>/images/marcus-p-655824-unsplash.png" alt="Card image cap">
                                            <div class="card-body">
                                              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                            </div>
                                        </div>
                                </div>
                                <div  class="col-md-4">
                                        <div  id="search-results" class="card" style="width: 15rem;">
                                            <img class="card-img-top" src="<?php echo get_bloginfo('template_directory'); ?>/images/marcus-p-655824-unsplash.png" alt="Card image cap">
                                            <div class="card-body">
                                              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                            </div>
                                        </div>
                                </div>
                                <div  class="col-md-4">
                                        <div  id="search-results" class="card" style="width: 15rem;">
                                            <img class="card-img-top" src="<?php echo get_bloginfo('template_directory'); ?>/images/marcus-p-655824-unsplash.png" alt="Card image cap">
                                            <div class="card-body">
                                              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                            </div>
                                        </div>
                                </div>
                                <div  class="col-md-4">
                                        <div  id="search-results" class="card" style="width: 15rem;">
                                            <img class="card-img-top" src="<?php echo get_bloginfo('template_directory'); ?>/images/marcus-p-655824-unsplash.png" alt="Card image cap">
                                            <div class="card-body">
                                              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                            </div>
                                        </div>
                                </div>
                                <div  class="col-md-4">
                                        <div  id="search-results" class="card" style="width: 15rem;">
                                            <img class="card-img-top" src="<?php echo get_bloginfo('template_directory'); ?>/images/marcus-p-655824-unsplash.png" alt="Card image cap">
                                            <div class="card-body">
                                              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                            </div>
                                        </div>
                                </div>
                                <a href="#">
                                    <button  id="view-all-search-results" class="btn-lg text-center">View More</button>
                                </a>
                        </div>
                        
                </div>
            </div>
    </section>

</body>
 

<div class="row" style="margin-top: 300px;  background-color: #242424">

    <!-- Grid column -->
    <div class="col-md-12 mb-4">

        <!--Footer-->
          <?php get_footer(); ?>
        <!--/.Footer-->

    </div>
    <!-- Grid column -->

</div>

</html>