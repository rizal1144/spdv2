<?php get_header('menu') ?> 

 <div id="light">
        <a class="boxclose" id="boxclose" onclick="lightbox_close();"></a>
          <iframe style="width: 100%; height: 100%;" src="https://www.youtube.com/embed/6whLKwynZNk"  frameborder="0"></iframe>
</div>
    <div id="fade" onClick="lightbox_close();"></div>


<body>
   
    
<div id="fh5co-started">
    <div class="videosection">
      <a href="#" onclick="lightbox_open();" style="cursor:pointer;">
        <div class="thumbHLN">
          <div class="vidthumb">
            <div class="parallax"></div>
          </div>
          
          <div id="play">
          
          </div>
        </div>
      </a>
    </div>
  </div>


  <section id="blog_area"> 
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-8">
          <div class="blog_left_sidebar">
            <article class="blog_style1">
              <div class="blog_img">
                <h1><b>Articles</b></h1>
                <img class="img-fluid" src="<?php echo get_bloginfo('template_directory'); ?>/images/bg-lambo.jpg" alt="">
              </div>
              <div class="blog_text">
                <div class="blog_text_inner">
                  <a href="single-blog.html"><h4>Lorem Ipsum</h4></a>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                  
              </div>
            </article>
            <article class="blog_style1">
              <div class="blog_img">
                <img class="img-fluid" src="<?php echo get_bloginfo('template_directory'); ?>/images/login1.png" alt="">
              </div>
              <div class="blog_text">
                <div class="blog_text_inner">
                  
                  <a href="single-blog.html"><h4>Lorem Ipsum</h4></a>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                  
              </div>
            </article>
            
            <article class="blog_style1">
              <div class="blog_img">
                <img class="img-fluid" src="<?php echo get_bloginfo('template_directory'); ?>/images/img-landing-header.png" alt="">
              </div>
              <div class="blog_text">
                <div class="blog_text_inner">
                 
                  <a href="single-blog.html"><h4>Lorem Ipsum</h4></a>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                  
              </div>
            </article>
            <div>
                <button class="btn btn-primary" style="background-color: black; width: 40%"><a href="#" style="padding: 10px 20px; font-size: 15px; color: white;">VIEW MORE</a></button> 
            </div>
            
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="blog_right_sidebar">
            <h1><b>Videos</b></h1>
            <div class="br"></div>
            
            <div class="nopadding b-padding video-container image-large">
              <iframe class="youtube-video" src="https://www.youtube.com/embed/JAtW5ULGQfs"frameborder="0" allowfullscreen></iframe>
              <aside class="single_sidebar_widget author_widget">
                <h6><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></h6>
              </aside>
            </div>

            <div class="nopadding b-padding video-container image-large">
              <iframe class="youtube-video" src="https://www.youtube.com/embed/iLX13XqqbyU"frameborder="0" allowfullscreen></iframe>
              <aside class="single_sidebar_widget author_widget">
                <h6><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></h6>
              </aside>
            </div>

            <div class="nopadding b-padding video-container image-large">
              <iframe class="youtube-video" src="https://www.youtube.com/embed/JAtW5ULGQfs"frameborder="0" allowfullscreen></iframe>
              <aside class="single_sidebar_widget author_widget">
                <h6><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></h6>
              </aside>
            </div>

            <div class="nopadding b-padding video-container image-large">
              <iframe class="youtube-video" src="https://www.youtube.com/embed/iLX13XqqbyU"frameborder="0" allowfullscreen></iframe>
              <aside class="single_sidebar_widget author_widget">
                <h6><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></h6>
              </aside>
            </div>

            <div class="nopadding b-padding video-container image-large">
              <iframe class="youtube-video" src="https://www.youtube.com/embed/JAtW5ULGQfs"frameborder="0" allowfullscreen></iframe>
              <aside class="single_sidebar_widget author_widget">
                <h6><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></h6>
              </aside>
            </div>

            <div class="nopadding b-padding video-container image-large">
              <iframe class="youtube-video" src="https://www.youtube.com/embed/iLX13XqqbyU"frameborder="0" allowfullscreen></iframe>
              <aside class="single_sidebar_widget author_widget">
                <h6><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></h6>
              </aside>
            </div>

            <div class="nopadding b-padding video-container image-large">
              <iframe class="youtube-video" src="https://www.youtube.com/embed/JAtW5ULGQfs"frameborder="0" allowfullscreen></iframe>
              <aside class="single_sidebar_widget author_widget">
                <h6><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></h6>
              </aside>
            </div>

            <div>
                <button class="btn btn-primary" style="background-color: black; width: 80%"><a href="#" style="padding: 10px 20px; font-size: 15px; color: white;">VIEW MORE</a></button> 
            </div>
          
         
        </div>
      </div>
    </div>
  </div>
</section>

<div id="demo" class="carousel slide" data-ride="carousel" style="margin-top: 100px">

  <!-- Indicators -->
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul>

  <!-- The slideshow -->
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="<?php echo get_bloginfo('template_directory'); ?>/images/blue.jpg" alt="Los Angeles">
    </div>
    <div class="carousel-item">
      <img src="<?php echo get_bloginfo('template_directory'); ?>/images/blue.jpg" alt="Chicago">
    </div>
    <div class="carousel-item">
      <img src="<?php echo get_bloginfo('template_directory'); ?>/images/blue.jpg" alt="New York">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>

</div>

<section class="section4" style="background-color:#404040;margin-top: -25px;">
    <style type="text/css" id="slider-css"></style>

    <div class="spe-cor">
        <div class="container">
            <h1 style="color: white; margin-bottom: -150px; padding-top: 20px;">Instagram Feed</h1>
            <div class="row">
                <div class="col-2">
                    <a id="custom-slider1" class="carousel-control-prev" href="#slider-2" role="button" data-slide="prev">
                        <img src="<?php echo get_bloginfo('template_directory'); ?>/images/Group 4.png">
                    </a>
                    <a id="custom-slider2" class="carousel-control-next" href="#slider-2" role="button" data-slide="next">
                        <img src="<?php echo get_bloginfo('template_directory'); ?>/images/Group 4-1.png">
                    </a>
                </div>
                <div class="col-11">
                    <div id="slider-2" class="carousel carousel-by-item slide" data-ride="carousel">
                        <div  class="carousel-inner" role="listbox">


                            <div class="carousel-item active">
                                <div class="col-md-4 col-sm-4 col-4" style="margin: 0px; padding: 0px;">
                                    <img class="d-block img-fluid" style="width: 100%; height: auto; " src="<?php echo get_bloginfo('template_directory'); ?>/images/jonathan-gallegos-725084-unsplash.png" alt="First slide">
                                </div>
                            </div>

                            <div id="padding-between-content1" class="carousel-item">
                                <div class="col-md-4 col-sm-4 col-4" style="margin: 0px; padding: 0px;">
                                    <img class="d-block img-fluid" style="width: 100%; height: auto;" src="<?php echo get_bloginfo('template_directory'); ?>/images/severin-d-370303-unsplash.png" alt="First slide">
                                </div>
                            </div>


                            <div id="padding-between-content2" class="carousel-item">
                                <div class="col-md-4 col-sm-4 col-4" style="margin: 0px; padding: 0px;">
                                    <img class="d-block img-fluid" style="width: 100%; height: auto;" src="<?php echo get_bloginfo('template_directory'); ?>/images/serge-kutuzov-223327-unsplash.png" alt="First slide">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-2">

                </div>

            </div>

        </div>
    </div>

</section>



<script>
    function GetUnique(e){var l=[],s=temp_c=[],t=["col-md-1","col-md-2","col-md-3","col-md-4","col-md-6","col-md-12","col-sm-1","col-sm-2","col-sm-3","col-sm-4","col-sm-6","col-sm-12","col-lg-1","col-lg-2","col-lg-3","col-lg-4","col-lg-6","col-lg-12","col-xs-1","col-xs-2","col-xs-3","col-xs-4","col-xs-6","col-xs-12","col-xl-1","col-xl-2","col-xl-3","col-xl-4","col-xl-6","col-xl-12"];$(e).each(function(){for(var l=$(e+" > div").attr("class").split(/\s+/),t=0;t<l.length;t++)s.push(l[t])});for(var c=0;c<s.length;c++)temp_c=s[c].split("-"),2==temp_c.length&&(temp_c.push(""),temp_c[2]=temp_c[1],temp_c[1]="xs",s[c]=temp_c.join("-")),-1==$.inArray(s[c],l)&&$.inArray(s[c],t)&&l.push(s[c]);return l}function setcss(e,l,s){for(var t=["","","","","",""],c=d=f=g=0,r=[1200,992,768,567,0],a="",o=[],a=0;a<e.length;a++){var i=e[a].split("-");if(3==i.length){switch(i[1]){case"xl":d=0;break;case"lg":d=1;break;case"md":d=2;break;case"sm":d=3;break;case"xs":d=4}t[d]=i[2]}}for(var n=0;n<t.length;n++)if(""!=t[n]){if(0==c&&(c=12/t[n]),f=12/t[n],g=100/f,a=s+" > .carousel-item.active.carousel-item-right,"+s+" > .carousel-item.carousel-item-next {-webkit-transform: translate3d("+g+"%, 0, 0);transform: translate3d("+g+", 0, 0);left: 0;}"+s+" > .carousel-item.active.carousel-item-left,"+s+" > .carousel-item.carousel-item-prev {-webkit-transform: translate3d(-"+g+"%, 0, 0);transform: translate3d(-"+g+"%, 0, 0);left: 0;}"+s+" > .carousel-item.carousel-item-left, "+s+" > .carousel-item.carousel-item-prev.carousel-item-right, "+s+" > .carousel-item.active {-webkit-transform: translate3d(0, 0, 0);transform: translate3d(0, 0, 0);left: 0;}",f>1){for(k=0;k<f-1;k++)o.push(l+" .cloneditem-"+k);o.length&&(a=a+o.join(",")+"{display: block;}"),o=[]}0!=r[n]&&(a="@media all and (min-width: "+r[n]+"px) and (transform-3d), all and (min-width:"+r[n]+"px) and (-webkit-transform-3d) {"+a+"}"),$("#slider-css").prepend(a)}$(l).each(function(){for(var e=$(this),l=0;l<c-1;l++)(e=e.next()).length||(e=$(this).siblings(":first")),e.children(":first-child").clone().addClass("cloneditem-"+l).appendTo($(this))})}

//Use Different Slider IDs for multiple slider
$(document).ready(function() {
    var item = '#slider-1 .carousel-item';
    var item_inner = "#slider-1 .carousel-inner";
    classes = GetUnique(item);
    setcss(classes, item, item_inner);
    
    
    var item_1 = '#slider-2 .carousel-item';
    var item_inner_1 = "#slider-2 .carousel-inner";
    classes = GetUnique(item_1);
    setcss(classes, item_1, item_inner_1);
});
</script>
</body>

<div class="row" style="margin-top: 200px;  background-color: #242424">

    <!-- Grid column -->
    <div class="col-md-12 mb-4">

        <?php get_footer(); ?>

    </div>
    <!-- Grid column -->

</div>


</html>