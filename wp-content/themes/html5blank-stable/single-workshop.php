
 <?php get_header(); ?>

	<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/plugin/lightslider-master/dist/css/lightslider.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/plugin/lightslider-master/dist/css/lightgallery.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/css/workshop-profile.css">
	
<div id="cover" class="column" style="height: 500px; padding-top: 0px;background-image: url('<?php the_field('add_background_cover');?>');">
		
</div>
<div class="columns">
  	<div class="column is-8">
   		<div id="profile-area">
   			<div id="card-profile-area" class="card">
					 <div class="card-content">
						 
						<!-- carousel area -->
							<ul id="lightSlider">
								<?php 

								$image = get_field('add_image');
								foreach($image as $idx => $row){
									echo "<li  data-thumb='" . $row['sizes']['large'] . "' data-src='" . $row['url'] . "'><img src='" . $row['sizes']['large'] ."'></li>";
								} 
								?>
							</ul>
						<!-- end of carousel area -->



  							<div id="profile-name">
  								<!-- echo workshop profile name -->
    							<p id=""class="title">
      								<strong><?php the_field('garage_name'); ?></strong>
    							</p>
    							<!-- end echo workshop profile name -->
    						</div>
    						<!-- echo workshop profile bio -->
    						<div class="profile-bio">
    						<p class="address"><strong>Address:</strong><?php the_field('address'); ?></p>
      							

    						
    						<p><strong>Hours:</strong> <?php the_field('opening_label') ?> Now, Closes&nbsp;<?php the_field('monday');?></p>
    							<div class="opening-hours">
    							
    								<table>
  						
									  <tr>
									    <td>Mon</td>
									    <td>&nbsp;:&nbsp;</td>
									    <?php 
									    	if(have_rows('opening_hours') ):
									    		while(have_rows('opening_hours')) : the_row();

									     ?>
									    <td><?php the_sub_field('monday_open')?> - <?php the_sub_field('monday_close'); ?></td>
									   <?php

									endwhile;
								endif;
									   ?>
									  </tr>
									  
									  <tr>
									    <td>Tue</td>
									    <td>&nbsp;:&nbsp;</td>
									    <?php 
									    	if(have_rows('opening_hours') ):
									    		while(have_rows('opening_hours')) : the_row();

									     ?>
									    <td><?php the_sub_field('tuesday_open');?> - <?php the_sub_field('tuesday_close');?></td>
									    <?php

									endwhile;
								endif;
									   ?>
									  </tr>
									  <tr>
									    <td>Wed</td>
									    <td>&nbsp;:&nbsp;</td>
									    <?php 
									    	if(have_rows('opening_hours') ):
									    		while(have_rows('opening_hours')) : the_row();

									     ?>
									    <td><?php the_sub_field('wednesday_open');?> - <?php the_sub_field('wednesday_close');?></td>
									    <?php

									endwhile;
								endif;
									   ?>
									  </tr>
									  <tr>
									    <td>Thu</td>
									    <td>&nbsp;:&nbsp;</td>
									    <?php 
									    	if(have_rows('opening_hours') ):
									    		while(have_rows('opening_hours')) : the_row();

									     ?>
									    <td><?php the_sub_field('thursday_open');?> - <?php the_sub_field('thursday_close');?></td>
									    <?php

									endwhile;
								endif;
									   ?>
									  </tr>
									  <tr>
									    <td>Fri</td>
									    <td>&nbsp;:&nbsp;</td>
									    <?php 
									    	if(have_rows('opening_hours') ):
									    		while(have_rows('opening_hours')) : the_row();

									     ?>
									    <td><?php the_sub_field('friday_open');?> - <?php the_sub_field('friday_close');?></td>
									    <?php

									endwhile;
								endif;
									   ?>
									  </tr>
									  <tr>
									    <td>Sat</td>
									    <td>&nbsp;:&nbsp;</td>
									    <?php 
									    	if(have_rows('opening_hours') ):
									    		while(have_rows('opening_hours')) : the_row();

									     ?>
									    <td><?php the_sub_field('saturday_open');?> - <?php the_sub_field('saturday_close');?></td>
									    <?php

									endwhile;
								endif;
									   ?>
									  </tr>
									  <tr>
									    <td>Sun</td>
									    <td>&nbsp;:&nbsp;</td>
									    <?php 
									    	if(have_rows('opening_hours') ):
									    		while(have_rows('opening_hours')) : the_row();

									     ?>
									    <td><?php the_sub_field('saturday_open');?> - <?php the_sub_field('saturday_close');?></td>
									    <?php

									endwhile;
								endif;
									   ?>
									  </tr>
									</table>
    							</div>
    							 
    							<div class="phone">
    								<p><strong>Phone&nbsp;&nbsp;:</strong>&nbsp;<?php the_field('phone'); ?></p>
    							</div>
    							<div class="service">
    								<p><strong>Service&nbsp;&nbsp;:</strong>&nbsp;<?php the_field('service') ?></p>
    							</div>

    							<!-- Review Area -->
    							<div class="review-title">
    								<h1><strong>REVIEWS&nbsp;:</strong></h1>
    							</div>
    							<article class="media">
								  <div class="media-content">
								    <div class="field">
								      <p class="control">
								        <textarea class="textarea" placeholder="Add a comment..."></textarea>
								      </p>
								    </div>
								    <nav class="level">
								      <div class="level-left">								        
								      </div>
								      <div class="level-right">
								        <div class="level-item">
								          <div class="level-item">
								          <a id="review-button" class="button is-info" style="background-color: #000;border-color: #000;border-radius:0;width: 200px; margin-bottom: 50px;">Submit</a>
								        </div>
								        </div>
								      </div>
								    </nav>
								  </div>
								</article>
								<div class="review-comment-area">

									<article class="media">									    	
									  <figure class="media-left">
									    <p class="image is-64x64">
									      <img class="is-rounded" src="https://bulma.io/images/placeholders/128x128.png">
									    </p>
									    
									  </figure>
									  <div class="media-content">
									    <div class="content">
									    	<nav class="level">
								      <div class="level-left">								        
								      </div>
								      <div class="level-right">
								        <div class="level-item">
								          <div class="level-item">
								          <a id="follow-button" class="button is-info" style="background-color: #000;border-color: #000;border-radius:0;width: 200px;">Follow</a>
								        </div>
								        </div>
								      </div>
								    </nav>
									      <p>
									        <strong>Barbara Middleton</strong>
									        <br>

									        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis porta eros lacus, nec ultricies elit blandit non. Suspendisse pellentesque mauris sit amet dolor blandit rutrum. Nunc in tempus turpis.
									        <br>
									        <small><a>Like</a> · <a>Reply</a> · 3 hrs</small>
									      </p>
									    </div>

									    
									  </div>
									</article>
									<article class="media">
									  
									  <div class="media-content">
									    <div class="field">
									      <p class="control">
									        <textarea class="textarea" placeholder="Add a comment..."></textarea>
									      </p>
									    </div>
									    <div class="field">
									      <p class="control">
									        <a id="review-button" class="button is-info" style="background-color: #000;border-color: #000;border-radius:0;width: 200px; margin-left: 38%;">See more comments</a>
									      </p>
									    </div>
									  </div>
									</article>
								</div>
    							<!-- End of Review Area -->
    						</div>
    						<!-- echo workshop profile bio -->
  						</div>
  
					</div>
   </div>
  </div>
  <div class="sidebar">
	  <div class="column">
	    <div class="card">
		  <div class="card-content">
		    <p class="sidebar1-title">
		      <strong>CHECK OTHER GARAGE</strong>
		    </p>
		  </div>
		  <div class="media">
		  	<div class="media-left">
		  		<figure class="image">
	          		<img  class="sidebar-image" src="https://via.placeholder.com/150x100.png" alt="Placeholder image">
	        	</figure>
		  	</div>
		  	<div class="media-right">
		  		<p><strong>Lorem Ipsum</strong></p>
		  		<div id="sidebar-content" class="content" >
		  			Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		  		</div>
		  	</div>
		  </div>
		  <div class="media">
		  	<div class="media-left">
		  		<figure class="image">
	          		<img  class="sidebar-image" src="https://via.placeholder.com/150x100.png" alt="Placeholder image">
	        	</figure>
		  	</div>
		  	<div class="media-right">
		  		<p><strong>Lorem Ipsum</strong></p>
		  		<div id="sidebar-content" class="content" >
		  			Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		  		</div>
		  	</div>
		  </div>
		  <div class="media">
		  	<div class="media-left">
		  		<figure class="image">
	          		<img  class="sidebar-image" src="https://via.placeholder.com/150x100.png" alt="Placeholder image">
	        	</figure>
		  	</div>
		  	<div class="media-right">
		  		<p><strong>Lorem Ipsum</strong></p>
		  		<div id="sidebar-content" class="content" >
		  			Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		  		</div>
		  	</div>
		  </div>
		  <div class="media">
		  	<div class="media-left">
		  		<figure class="image">
	          		<img  class="sidebar-image" src="https://via.placeholder.com/150x100.png" alt="Placeholder image">
	        	</figure>
		  	</div>
		  	<div class="media-right">
		  		<p><strong>Lorem Ipsum</strong></p>
		  		<div id="sidebar-content" class="content" >
		  			Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		  		</div>
		  	</div>
		  </div>
		</div>
		</div>

	  <div class="sidebar2">
		  <div class="card">
		  	<div class="card-content">
		  		<div class="media">
		  			<div class="media-left">
		  				<p class="image is-96x96">
		  					<img class="is-rounded" src="https://via.placeholder.com/96x96.png">
		  					<span class="icon has-text-success">
  								<i  id="icon-image" class="fas fa-check-circle"></i>
							</span>
		  				</p>
		  			</div>
		  			<div class="media-right">
		  				<p><strong>John Doe</strong></p>
			  				<div  id="sidebar-content" class="content" >
			  				Lorem ipsum dolor sit amet, consectetur adipiscing elit.
			  				<a  id="follow-sidebar-button" class="button is-black is-small">Follow</a>
		  				</div>

		  			</div>
		  			
		  		</div>
		  		<div class="media">
		  			<div class="media-left">
		  				<p class="image is-96x96">
		  					<img class="is-rounded" src="https://via.placeholder.com/96x96.png">
		  					<span class="icon has-text-success">
  								<i  id="icon-image" class="fas fa-check-circle"></i>
							</span>
		  				</p>
		  			</div>
		  			<div class="media-right">
		  				<p><strong>John Doe</strong></p>
			  				<div  id="sidebar-content" class="content" >
			  				Lorem ipsum dolor sit amet, consectetur adipiscing elit.
			  				<a  id="follow-sidebar-button" class="button is-black is-small">Follow</a>
		  				</div>

		  			</div>
		  			
		  		</div>
		  		<div class="media">
		  			<div class="media-left">
		  				<p class="image is-96x96">
		  					<img class="is-rounded" src="https://via.placeholder.com/96x96.png">
		  					<span class="icon has-text-success">
  								<i  id="icon-image" class="fas fa-check-circle"></i>
							</span>
		  				</p>
		  			</div>
		  			<div class="media-right">
		  				<p><strong>John Doe</strong></p>
			  				<div  id="sidebar-content" class="content" >
			  				Lorem ipsum dolor sit amet, consectetur adipiscing elit.
			  				<a  id="follow-sidebar-button" class="button is-black is-small">Follow</a>
		  				</div>

		  			</div>
		  			
		  		</div>
		  	</div>
		  </div>
	  </div>
	  </div>  
	</div>
</div>

<script type="text/javascript" src="<?php echo get_bloginfo('template_directory'); ?>/js/jquery.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/plugin/lightslider-master/dist/js/lightslider.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/plugin/lightslider-master/dist/js/lightgallery-all.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#lightSlider').lightSlider({
        item:4,
        loop:false,
        slideMove:2,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed:600,
        pager:false,
        enableDrag: false,
         gallery:true,
        responsive : [
            {
                breakpoint:800,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:-1,
                  }
            },
            {
                breakpoint:480,
                settings: {
                    item:2,
                    slideMove:1
                  }
            }
        ],
        onSliderLoad: function(el) {
            el.lightGallery({
                selector: '#lightSlider .lslide'
            });
            el.addClass('no-margin-slider');
            $(".no-margin-slider").closest(".lSSlideOuter").addClass("padding-right-slider");
        },
        prevHtml: "<i class='fa fa-chevron-left'></i>",
        nextHtml: "<i class='fa fa-chevron-right'></i>"
    });  
  });
</script>
 <?php get_footer(); ?>