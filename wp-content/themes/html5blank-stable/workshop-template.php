<?php

/*
	Template Name: Workshops Profile
*/ 

?>

<?php get_header(); ?>

<?php  

	$workshop_post =  get_the_ID();
?>


<div id="cover" class="column" style="height: 500px; padding-top: 0px;">
			
</div>	
		
	<!-- https://via.placeholder.com/64x64.png -->
	<div class="columns">
  	<div class="column is-8">
   		<div id="profile-area">
   			<div id="card-profile-area" class="card">
					 <div class="card-content">
						 
						<!-- carousel area -->

						<!-- end of carousel area -->


  							<div id="profile-name">
  								<!-- echo workshop profile name -->
    							<p id=""class="title">
      								<strong><?php  the_field('garage_name', '$workshop_post') ?></strong>
    							</p>
    							<!-- end echo workshop profile name -->
    						</div>
    						<!-- echo workshop profile bio -->
    						<div class="profile-bio">
    						<p class="address">
      							<strong>Address:</strong>  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
    						</p>
    						<p><strong>Hours:</strong> Open Now, Closes 9 PM</p>
    							<div class="opening-hours">
    							
    								<table>
  
									  <tr>
									    <td>Mon</td>
									    <td>&nbsp;:&nbsp;</td>
									    <td>10:00 am - 18:00 pm</td>
									  </tr>
									  <tr>
									    <td>Tue</td>
									    <td>&nbsp;:&nbsp;</td>
									    <td>10:00 am - 18:00 pm</td>
									  </tr>
									  <tr>
									    <td>Wed</td>
									    <td>&nbsp;:&nbsp;</td>
									    <td>10:00 am - 18:00 pm</td>
									  </tr>
									  <tr>
									    <td>Thu</td>
									    <td>&nbsp;:&nbsp;</td>
									    <td>10:00 am - 18:00 pm</td>
									  </tr>
									  <tr>
									    <td>Fri</td>
									    <td>&nbsp;:&nbsp;</td>
									    <td>10:00 am - 18:00 pm</td>
									  </tr>
									  <tr>
									    <td>Sat</td>
									    <td>&nbsp;:&nbsp;</td>
									    <td>10:00 am - 18:00 pm</td>
									  </tr>
									  <tr>
									    <td>Sun</td>
									    <td>&nbsp;:&nbsp;</td>
									    <td>10:00 am - 18:00 pm</td>
									  </tr>
									</table>
    							</div>
    							<div class="phone">
    								<p><strong>Phone&nbsp;&nbsp;:</strong>&nbsp;(021) 123 123 123</p>
    							</div>
    							<div class="service">
    								<p><strong>Service&nbsp;&nbsp;:</strong>&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
    							</div>

    							<!-- Review Area -->
    							<div class="review-title">
    								<h1><strong>REVIEWS&nbsp;:</strong></h1>
    							</div>
    							<article class="media">
								  <div class="media-content">
								    <div class="field">
								      <p class="control">
								        <textarea class="textarea" placeholder="Add a comment..."></textarea>
								      </p>
								    </div>
								    <nav class="level">
								      <div class="level-left">								        
								      </div>
								      <div class="level-right">
								        <div class="level-item">
								          <div class="level-item">
								          <a id="review-button" class="button is-info" style="background-color: #000;border-color: #000;border-radius:0;width: 200px; margin-bottom: 50px;">Submit</a>
								        </div>
								        </div>
								      </div>
								    </nav>
								  </div>
								</article>
								<div class="review-comment-area">

									<article class="media">									    	
									  <figure class="media-left">
									    <p class="image is-64x64">
									      <img class="is-rounded" src="https://bulma.io/images/placeholders/128x128.png">
									    </p>
									    
									  </figure>
									  <div class="media-content">
									    <div class="content">
									    	<nav class="level">
								      <div class="level-left">								        
								      </div>
								      <div class="level-right">
								        <div class="level-item">
								          <div class="level-item">
								          <a id="follow-button" class="button is-info" style="background-color: #000;border-color: #000;border-radius:0;width: 200px;">Follow</a>
								        </div>
								        </div>
								      </div>
								    </nav>
									      <p>
									        <strong>Barbara Middleton</strong>
									        <br>

									        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis porta eros lacus, nec ultricies elit blandit non. Suspendisse pellentesque mauris sit amet dolor blandit rutrum. Nunc in tempus turpis.
									        <br>
									        <small><a>Like</a> · <a>Reply</a> · 3 hrs</small>
									      </p>
									    </div>

									    
									  </div>
									</article>
									<article class="media">
									  
									  <div class="media-content">
									    <div class="field">
									      <p class="control">
									        <textarea class="textarea" placeholder="Add a comment..."></textarea>
									      </p>
									    </div>
									    <div class="field">
									      <p class="control">
									        <a id="review-button" class="button is-info" style="background-color: #000;border-color: #000;border-radius:0;width: 200px; margin-left: 38%;">See more comments</a>
									      </p>
									    </div>
									  </div>
									</article>
								</div>
    							<!-- End of Review Area -->
    						</div>
    						<!-- echo workshop profile bio -->
  						</div>
  
	
					</div>
   </div>
  </div>

  <div class="sidebar">
	  <div class="column">
	    <div class="card">
		  <div class="card-content">
		    <p class="sidebar1-title">
		      <strong>CHECK OTHER GARAGE</strong>
		    </p>
		  </div>
		  <div class="media">
		  	<div class="media-left">
		  		<figure class="image">
	          		<img  class="sidebar-image" src="https://via.placeholder.com/150x100.png" alt="Placeholder image">
	        	</figure>
		  	</div>
		  	<div class="media-right">
		  		<p><strong>Lorem Ipsum</strong></p>
		  		<div id="sidebar-content" class="content" >
		  			Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		  		</div>
		  	</div>
		  </div>
		  <div class="media">
		  	<div class="media-left">
		  		<figure class="image">
	          		<img  class="sidebar-image" src="https://via.placeholder.com/150x100.png" alt="Placeholder image">
	        	</figure>
		  	</div>
		  	<div class="media-right">
		  		<p><strong>Lorem Ipsum</strong></p>
		  		<div id="sidebar-content" class="content" >
		  			Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		  		</div>
		  	</div>
		  </div>
		  <div class="media">
		  	<div class="media-left">
		  		<figure class="image">
	          		<img  class="sidebar-image" src="https://via.placeholder.com/150x100.png" alt="Placeholder image">
	        	</figure>
		  	</div>
		  	<div class="media-right">
		  		<p><strong>Lorem Ipsum</strong></p>
		  		<div id="sidebar-content" class="content" >
		  			Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		  		</div>
		  	</div>
		  </div>
		  <div class="media">
		  	<div class="media-left">
		  		<figure class="image">
	          		<img  class="sidebar-image" src="https://via.placeholder.com/150x100.png" alt="Placeholder image">
	        	</figure>
		  	</div>
		  	<div class="media-right">
		  		<p><strong>Lorem Ipsum</strong></p>
		  		<div id="sidebar-content" class="content" >
		  			Lorem ipsum dolor sit amet, consectetur adipiscing elit.
		  		</div>
		  	</div>
		  </div>
		</div>
		</div>

	  <div class="sidebar2">
		  <div class="card">
		  	<div class="card-content">
		  		<div class="media">
		  			<div class="media-left">
		  				<p class="image is-96x96">
		  					<img class="is-rounded" src="https://via.placeholder.com/96x96.png">
		  					<span class="icon has-text-success">
  								<i  id="icon-image" class="fas fa-check-circle"></i>
							</span>
		  				</p>
		  			</div>
		  			<div class="media-right">
		  				<p><strong>John Doe</strong></p>
			  				<div  id="sidebar-content" class="content" >
			  				Lorem ipsum dolor sit amet, consectetur adipiscing elit.
			  				<a  id="follow-sidebar-button" class="button is-black is-small">Follow</a>
		  				</div>

		  			</div>
		  			
		  		</div>
		  		<div class="media">
		  			<div class="media-left">
		  				<p class="image is-96x96">
		  					<img class="is-rounded" src="https://via.placeholder.com/96x96.png">
		  					<span class="icon has-text-success">
  								<i  id="icon-image" class="fas fa-check-circle"></i>
							</span>
		  				</p>
		  			</div>
		  			<div class="media-right">
		  				<p><strong>John Doe</strong></p>
			  				<div  id="sidebar-content" class="content" >
			  				Lorem ipsum dolor sit amet, consectetur adipiscing elit.
			  				<a  id="follow-sidebar-button" class="button is-black is-small">Follow</a>
		  				</div>

		  			</div>
		  			
		  		</div>
		  		<div class="media">
		  			<div class="media-left">
		  				<p class="image is-96x96">
		  					<img class="is-rounded" src="https://via.placeholder.com/96x96.png">
		  					<span class="icon has-text-success">
  								<i  id="icon-image" class="fas fa-check-circle"></i>
							</span>
		  				</p>
		  			</div>
		  			<div class="media-right">
		  				<p><strong>John Doe</strong></p>
			  				<div  id="sidebar-content" class="content" >
			  				Lorem ipsum dolor sit amet, consectetur adipiscing elit.
			  				<a  id="follow-sidebar-button" class="button is-black is-small">Follow</a>
		  				</div>

		  			</div>
		  			
		  		</div>
		  	</div>
		  </div>
	  </div>
	  </div>  
	</div>
</div>

<?php get_footer(); ?>